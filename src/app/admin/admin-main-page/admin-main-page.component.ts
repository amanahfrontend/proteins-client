import {Router} from '@angular/router';
import {Component, OnInit, Input} from '@angular/core';
import {Message} from "primeng/components/common/message";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-admin-main-page',
  templateUrl: './admin-main-page.component.html',
  styleUrls: ['./admin-main-page.component.css', '../../customer/existing-customer/existing-customer.component.css']
})

export class AdminMainPageComponent implements OnInit {
  getDataForType: any;
  items:any[]=[];
  activeTab: string;
  getUserDataFlag: boolean;
  msg: Message[];
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private router: Router) {
     translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    if (this.lang == "ar")
    {
      this.items = [{name:'نوع العميل',key:'CustType'}, {name:'نوع المكالمة',key:'CallType'},{name:'اولوية المكالمة',key:'CallPriority'},
                  {name: 'نوع التليفون',key:'PhoneType'}, {name:'الحالة',key:'ActionStatus'},{name:'نوع العقد',key:'ContractType'}, 
                  {name:'نوع الطلب',key:'OrderType'},{name:'اولوية الطلب',key:'OrderPriority'},{name:'حالة الطلب',key:'OrderStatus'}, 
                  {name:'مشاكل الطلب',key:'OrderProb'},{name:'معدل تقدم الطلبات',key:'OrderProg'} ];
    }
    else if (this.lang == "en")
    {
      this.items = [{name:'Customer Type',key:'CustType'}, {name:'Call Type',key:'CallType'},{name:'Call Priority',key:'CallPriority'},
                    {name: 'Phone Type',key:'PhoneType'}, {name:'Action Status',key:'ActionStatus'},{name:'Contract Type',key:'ContractType'}, 
                    {name:'Order Type',key:'OrderType'},{name:'Order Priority',key:'OrderPriority'},{name:'Order Status',key:'OrderStatus'}, 
                    {name:'Order Problems',key:'OrderProb'},{name:'Order Progress',key:'OrderProg'} ];  
    }
    debugger;
    this.getUserDataFlag = false;
    this.activeTab = 'data';
    this.getData('CustType');
  }

  setActiveTab(tabName) {
    this.activeTab = tabName;
    if (tabName == 'users') {
      this.getUserDataFlag = true;
    } else if (tabName == 'upload') {
      this.getUserDataFlag = false;
    } else if (tabName == 'assign-tec') {
      this.getUserDataFlag = false;
    } else if (tabName == 'settingDispatchers') {
      this.getUserDataFlag = false;
    } else if (tabName == 'data') {
      this.getUserDataFlag = false;
    }
  }

  applyDefaultActiveClass(item) {
    if (this.items.indexOf(item) == 0) {
      return 'uk-open'
    }
  }

  getData(type) {
    this.getDataForType = type;
    console.log(this.getDataForType);
  }

}
