import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUploadResourcesComponent } from './admin-upload-resources.component';

describe('AdminUploadResourcesComponent', () => {
  let component: AdminUploadResourcesComponent;
  let fixture: ComponentFixture<AdminUploadResourcesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUploadResourcesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUploadResourcesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
