import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from 'primeng/components/common/api';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-admin-upload-resources',
  templateUrl: './admin-upload-resources.component.html',
  styleUrls: ['./admin-upload-resources.component.css']
})

export class AdminUploadResourcesComponent implements OnInit {
  uploadedFiles: any[] = [];
  toggleItemsUploadButtons: boolean;
  toggleSalaryUploadButtons: boolean;
  itemsError: Message[] = [];
  salaryError: Message[] = [];
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private lookup: LookupService, private messageService: MessageService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.toggleItemsUploadButtons = false;
    this.toggleSalaryUploadButtons = false;
    this.salaryError = [];
  }

  showAtView(event, type) {
    //console.log('select');
    this.uploadedFiles = [];
    this.uploadedFiles.push(event.files[0]);
    if (type == 'items') {
      this.toggleItemsUploadButtons = true;
    } else if (type == 'salary') {
      this.toggleSalaryUploadButtons = true;
    }
  }

  onUpload(event, type) {
    //console.log('upload');
    let files = event.files;
    let file = files[0];

    if (file) {
      let reader = new FileReader();
      let base64file;

      reader.onload = (readerEvt) => {
        let binaryStringFileReader = <FileReader>readerEvt.target;
        let binaryString = binaryStringFileReader.result;
        // document.getElementById("base64textarea").value = btoa(binaryString);
        base64file = btoa(binaryString);
        //console.log(base64file);

        /*********** collect the json object *************/
        let jsonObject = {
          "fileName": file.name,
          "fileContent": base64file
        };
        //console.log(jsonObject);
        if (type == 'items') {
          this.lookup.postItemsExcelSheet(jsonObject).subscribe(() => {
              this.toggleItemsUploadButtons = false;
              this.itemsError = [];
              this.itemsError.push({
                severity: this.translate.instant('MSG.suces'),
                summary: this.translate.instant('MSG.sucesful'),
                detail: this.translate.instant('MSG.Excel_sheet_uploaded_successfully')
              });
            },
            err => {
              this.toggleItemsUploadButtons = true;
              this.itemsError = [];
              this.itemsError.push({
                severity: this.translate.instant('MSG.Error_Msg'),
                summary: this.translate.instant('MSG.fail'),
                detail: this.translate.instant('MSG.General_Failed_Msg_detail')
              });
            });
        } else if (type == 'salary') {
          this.lookup.postSalaryExcelSheet(jsonObject).subscribe(() => {
              this.toggleSalaryUploadButtons= false;
              this.salaryError = [];
              this.salaryError.push({
                severity: this.translate.instant('MSG.suces'),
                summary: this.translate.instant('MSG.sucesful'),
                detail: this.translate.instant('MSG.Excel_sheet_uploaded_successfully')
              });
            },
            err => {
              this.toggleSalaryUploadButtons = true;
              this.salaryError = [];
              this.salaryError.push({
                severity: this.translate.instant('MSG.Error_Msg'),
                summary: this.translate.instant('MSG.fail'),
                detail: this.translate.instant('MSG.General_Failed_Msg_detail')
              });
            });
        }

        /***************************************************/
      };

      /* Not sure about the importance of this libe :( */
      let x = reader.readAsBinaryString(file);
      //console.log(x);
    }
  };

}
