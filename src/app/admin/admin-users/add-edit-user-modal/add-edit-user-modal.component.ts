import { Component, OnInit, Input, OnDestroy, OnChanges } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LookupService } from "../../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";

@Component({
  selector: 'app-add-edit-user-modal',
  templateUrl: './add-edit-user-modal.component.html',
  styleUrls: ['./add-edit-user-modal.component.css']
})
export class AddEditUserModalComponent implements OnInit, OnChanges {
  @Input() data: any;
  @Input() header: string;
  existedUserFlag: boolean;
  passwordRegex: any;
  emailRegex: any;
  roles: string[];
  rolesSubscription: Subscription;
  toggleLoading: boolean;

  constructor(public activeModal: NgbActiveModal, private lookup: LookupService) {
  }

  ngOnInit() {
    this.toggleLoading = true;

    this.rolesSubscription = this.lookup.getRoles().subscribe(
      (roles) => {
        this.roles = roles;
        this.toggleLoading = false;
      },
      err => {

      });

    this.data.roleNames = this.data.roleNames && this.data.roleNames[0];
    //console.log(this.data);
    this.existedUserFlag = JSON.stringify(this.data) != JSON.stringify({});
    this.passwordRegex = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    this.emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  }

  ngOnChanges() {
    //console.log(this.data);
  }

  close(result) {
    this.activeModal.close(result);
  }

}
