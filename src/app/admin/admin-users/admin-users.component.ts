import { Component, Input, OnChanges, OnDestroy } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MessageService } from "primeng/components/common/messageservice";
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { AddEditUserModalComponent } from "./add-edit-user-modal/add-edit-user-modal.component";
import { AdminChangePasswordComponent } from "./admin-change-password/admin-change-password.component";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: "app-admin-users",
  templateUrl: "./admin-users.component.html",
  styleUrls: ["./admin-users.component.css"]
})

export class AdminUsersComponent implements OnDestroy, OnChanges {
  @Input() getData: boolean;
  users: any[];
  UsersSubscription: Subscription;
  modalRef: any;
  toggleLoading;
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private lookup: LookupService, private messageService: MessageService, private modalService: NgbModal) {
    translate.setDefaultLang(this.lang);
  }

  // ngOnInit() {
  //   this.toggleLoading = true;
  // }

  ngOnChanges() {
    this.toggleLoading = true;

    if (this.getData) {
      this.UsersSubscription = this.lookup.getUsers().subscribe(
        (users) => {
          this.toggleLoading = false;
          this.users = users;
          this.users.map((user) => {
            user.name = `${user.firstName} ${user.middleName} ${user.lastName}`;
          });
          //console.log('got users');
        },
        err => {
          this.toggleLoading = false;

          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.General_Failed_Msg_detail')
          });
        });
    }
  }

  ngOnDestroy() {
    this.UsersSubscription && this.UsersSubscription.unsubscribe();
  }

  edit(user) {
    let userCopy = Object.assign({}, user);

    this.openAddEditUserModal(userCopy, `${this.translate.instant('ADMN.Edit')} ${userCopy.name}`);

    this.modalRef.result.then(
      (editedData) => {
        let userRoles = [];
        userRoles.push(editedData.roleNames);
        editedData.roleNames = userRoles;

        delete editedData.name;
        console.log(editedData);
        console.log(editedData.roleNames);

        this.lookup.updateUser(editedData).subscribe(
          () => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: `${user.name} ${this.translate.instant('MSG.editSavSuces')}`
            });
            user = editedData;
          },
          err => {
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: this.translate.instant('MSG.info'),
          summary: this.translate.instant('MSG.nothingEdit'),
          detail: `${this.translate.instant('MSG.No_edits_saved_to')} ${user.name}`
        });
      });
  }

  changeUserPassword(user) {
    let userCopy = Object.assign({}, user);
    this.openModalChangePassword(userCopy, `${this.translate.instant("ADMN.Change_Password_of")} ${userCopy.name} `);

  }

  remove(user) {
    //console.log(user);
    this.lookup.deleteUser(user.userName).subscribe(
      () => {
        this.messageService.add({
          severity: this.translate.instant('MSG.suces'),
          summary: this.translate.instant('MSG.sucesful'),
          detail: this.translate.instant('MSG.usrDeletSuces')
        });

        this.users = this.users.filter((oneUser) => {
          return oneUser.userName !== user.userName;
        });
      },
      err => {
        this.messageService.add({
          severity: this.translate.instant('MSG.Error_Msg'),
          summary: this.translate.instant('MSG.fail'),
          detail: this.translate.instant('MSG.failNetworkRemovDesc')
        });
      });
  }

  add() {
    this.openAddEditUserModal({}, `${this.translate.instant("ADMN.Add_New")}`);

    this.modalRef.result.then(
      (newUser) => {
        let userRoles = [];
        //console.log(newUser.roleNames);
        userRoles.push(newUser.roleNames);
        newUser.roleNames = userRoles;
        //console.log(newUser.roleNames);

        this.lookup.postNewUser(newUser).subscribe(
          (resUser) => {
            console.log(resUser);

            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });

            resUser.name = resUser.firstName + resUser.middleName + resUser.lastName;
            this.users.push(resUser);
          },
          err => {
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkNewUsrDesc')
            });
          });
      })
      .catch(() => {
        this.messageService.add({
          severity: this.translate.instant('MSG.info'),
          summary: this.translate.instant('MSG.nothingEdit'),
          detail: this.translate.instant('MSG.usrSavCancl')
        });
      });
  }

  openAddEditUserModal(data, header) {
    this.modalRef = this.modalService.open(AddEditUserModalComponent);
    this.modalRef.componentInstance.header = header;
    // this.modalRef.componentInstance.type = 'prompt';
    this.modalRef.componentInstance.data = data;
  }

  openModalChangePassword(data, header) {
    this.modalRef = this.modalService.open(AdminChangePasswordComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.data = data;
  }
}
