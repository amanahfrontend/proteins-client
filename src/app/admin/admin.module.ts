import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { MultiSelectModule } from "primeng/components/multiselect/multiselect";
import { SharedModuleModule } from "../shared-module/shared-module.module";
import { AdminMainPageComponent } from "./admin-main-page/admin-main-page.component";
import { AdminUploadResourcesComponent } from "./admin-upload-resources/admin-upload-resources.component";
import { AdminUsersComponent } from "./admin-users/admin-users.component";
import { AssignTecComponent } from "./assign-tec/assign-tec.component";
import { EditDisAndProbModalComponent } from "./setting-dispatchers/edit-dis-and-prob-modal/edit-dis-and-prob-modal.component";
import { SettingDispatchersComponent } from "./setting-dispatchers/setting-dispatchers.component";
import { SimpleAdminContentComponent } from "./simple-admin-content/simple-admin-content.component";
import { AdminChangePasswordComponent } from "./admin-users/admin-change-password/admin-change-password.component";
import { UserService } from "./services/user.service";
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from "@angular/common/http";
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
const routes: Routes = [
  {
    path: "",
    component: AdminMainPageComponent,
    // canActivate: [AuthGuardGuard],
    data: { roles: ["Admin"] }
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModuleModule,
    MultiSelectModule,
    RouterModule.forChild(routes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    AdminMainPageComponent, SimpleAdminContentComponent,
    AdminUsersComponent, AdminUploadResourcesComponent,
    AssignTecComponent, SettingDispatchersComponent,
    EditDisAndProbModalComponent, AdminChangePasswordComponent
  ],
  entryComponents: [
    EditDisAndProbModalComponent,
    AdminChangePasswordComponent
  ],
  providers: [UserService]
})
export class AdminModule {
}
