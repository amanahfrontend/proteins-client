import {Component, OnInit, OnDestroy} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {DragulaService} from 'ng2-dragula';
import {Message} from "primeng/components/common/message";
import {Subscription} from "rxjs";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-assign-tec',
  templateUrl: './assign-tec.component.html',
  styleUrls: ['./assign-tec.component.css']
})

export class AssignTecComponent implements OnInit, OnDestroy {
  dispatchers: any[];
  tecs: any[];
  tecToPost: any;
  disToPost: any;
  activeDis: any;
  toggleLoading: boolean;
  tecLoading: boolean;
  toggleRemoveTecButtons: boolean;
  getDispatchersSubscription: Subscription;
  getTecsByIdSubscription: Subscription;
  getTecsSubscription: Subscription;
  removeSubscription: Subscription;
  cornerMessage: Message[];
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookup: LookupService, private dragulaService: DragulaService) {
    translate.setDefaultLang(this.lang);
  }

  postTecByDis(val) {
    // console.log(val[1].classList.contains('dispatcher-card'));
    this.tecLoading = true;
    console.log(this.tecToPost);
    console.log(this.disToPost);
    setTimeout(() => {
      let tecWithDis = {
        fK_technican_id: this.tecToPost.id,
        fK_dispatcher_id: this.disToPost.id
      };
      if (val[1].classList.contains('dispatcher-card')) {
        console.log(tecWithDis);
        this.lookup.postTecnichanWithDis(tecWithDis).subscribe(() => {
            this.cornerMessage = [];
            this.cornerMessage.push({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: `${this.translate.instant('ADMN.TECHN')} ${this.tecToPost.firstName} ${this.tecToPost.middleName} ${this.translate.instant("MSG.Assigned_to_dispatcher")} ${this.disToPost.firstName} ${this.disToPost.middleName}`
            });
            this.tecLoading = false;
          },
          err => {
            this.cornerMessage = [];
            this.cornerMessage.push({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant("MSG.failAsinTec")
            });
            this.tecLoading = false;
          })
      }
    }, 200)
  }

  ngOnInit() {
    // this.tecs = [];
    // this.dispatchers = [];
    this.toggleLoading = true;
    this.getDispatchers();
    this.getTecs();

    this.dragulaService.setOptions('tecs-drag', {
      removeOnSpill: false,
      copySortSource: false,
      revertOnSpill: true,
      accepts: (item) => {
        return item.classList.contains('tec-name');
      }
    });

    this.dragulaService.drop.subscribe((value) => {
      // console.log(value);
      this.postTecByDis(value.slice(1));
    });
  }

  ngOnDestroy() {
    this.dragulaService.destroy('tecs-drag');
    this.getTecsSubscription.unsubscribe();
    this.getTecsByIdSubscription && this.getTecsByIdSubscription.unsubscribe();
    this.getDispatchersSubscription.unsubscribe();
    this.removeSubscription && this.removeSubscription.unsubscribe();
  }

  setActiveDis(dis) {
    this.activeDis = dis;
  }

  setTecToPost(tec) {
    this.tecToPost = tec;
  }

  setDisToPost(dis) {
    console.log('assign dis');
    this.disToPost = dis;
  }

  getTecs() {
    this.getTecsSubscription = this.lookup.getTecs().subscribe((tecs) => {
        this.tecs = tecs;
        console.log(tecs);
        this.toggleRemoveTecButtons = false;
        this.activeDis = null;
        if (this.dispatchers) {
          this.toggleLoading = false;
        }
      },
      err => {
        this.toggleLoading = false;
      }
    )
  };

  getTecsByDis(dis) {
    this.tecLoading = true;

    this.getTecsByIdSubscription = this.lookup.getTecsByDis(dis.id).subscribe((tecs) => {
        this.tecs = tecs;
        this.toggleRemoveTecButtons = true;
        this.setActiveDis(dis);
        this.tecLoading = false;
      },
      err => {
        this.tecLoading = false;
      }
    )
  };

  removeTec(id) {
    this.tecLoading = true;
    this.removeSubscription = this.lookup.removeTecFromDis(id).subscribe(() => {
        this.tecs = this.tecs.filter((tec) => {
          return tec.id != id;
        });
        this.tecLoading = false;
      },
      err => {
        console.log(err);
        this.tecLoading = false;
        // this.toggleLoading = false;
      }
    )
  };

  getDispatchers() {
    this.getDispatchersSubscription = this.lookup.getDispatchers().subscribe((dispatchers) => {
        this.dispatchers = dispatchers;
        console.log(dispatchers);
        if (this.tecs) {
          this.toggleLoading = false;
        }
      },
      err => {
        this.toggleLoading = false;
      })
  }

}
