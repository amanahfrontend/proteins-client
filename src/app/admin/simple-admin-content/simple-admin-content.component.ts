import {Component, OnInit, Input, OnChanges, OnDestroy} from '@angular/core';
import {Subscription} from "rxjs";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {MessageService} from 'primeng/components/common/messageservice';
import {PromptComponent} from '../../shared-module/shared/prompt/prompt.component'
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-simple-admin-content',
  templateUrl: './simple-admin-content.component.html',
  styleUrls: ['./simple-admin-content.component.css']
})
export class SimpleAdminContentComponent implements OnInit, OnChanges, OnDestroy {
  @Input() type: any;
  @Input() getData: string;
  dataSubscription: Subscription;
  deleteDataSubscription: Subscription;
  modalRef: any;
  rows: any[];
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookUp: LookupService, private messageService: MessageService, private modalService: NgbModal, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.rows = [];
    // this.toggleLoading = true;
  }

  ngOnChanges() {
    this.toggleLoading = true;
    !this.rows && (this.rows = []);
    if (this.type.key == 'CustType' && this.getData == 'CustType') {
      this.dataSubscription = this.lookUp.getCustomerTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'CallType' && this.getData == 'CallType') {
      this.dataSubscription = this.lookUp.getCallsTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'CallPriority' && this.getData == 'CallPriority') {
      this.dataSubscription = this.lookUp.getPriorities().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'PhoneType' && this.getData == 'PhoneType') {
      this.dataSubscription = this.lookUp.getPhoneTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'ActionStatus' && this.getData == 'ActionStatus') {
      this.dataSubscription = this.lookUp.getStatues().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'ContractType' && this.getData == 'ContractType') {
      this.dataSubscription = this.lookUp.getContractTypes().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'Role' && this.getData == 'Role') {
      this.dataSubscription = this.lookUp.getRoles().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'OrderType' && this.getData == 'OrderType') {
      this.dataSubscription = this.lookUp.getOrderType().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'OrderPriority' && this.getData == 'OrderPriority') {
      this.dataSubscription = this.lookUp.getOrderPriority().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'OrderStatus' && this.getData == 'OrderStatus') {
      this.dataSubscription = this.lookUp.getOrderStatus().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          //console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'OrderProg' && this.getData == 'OrderProg') {
      this.dataSubscription = this.lookUp.getAllOrderProgress().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
    else if (this.type.key == 'OrderProb' && this.getData == 'OrderProb') {
      this.dataSubscription = this.lookUp.getAllOrderProblems().subscribe((data) => {
          this.toggleLoading = false;
          this.rows = data;
          console.log(this.rows);
        },
        err => {
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkUpdtDesc')
          });
          this.toggleLoading = false;
        })
    }
  }

  ngOnDestroy() {
    this.dataSubscription && this.dataSubscription.unsubscribe();
    this.deleteDataSubscription && this.deleteDataSubscription.unsubscribe();
  }

  add() {
    debugger;
    this.openModal({}, `${this.translate.instant('ADMN.Add_New')} ${this.type.name}`, this.type);
    this.modalRef.result.then((newValue) => {
      // newValue = {
      //   name: newValue.name
      // };
      console.log(newValue);
      if (this.type.key == 'CustType') {
        this.dataSubscription = this.lookUp.postCustomerTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })

      }
      else if (this.type.key == 'CallType') {
        this.dataSubscription = this.lookUp.postCallsTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'CallPriority') {
        this.dataSubscription = this.lookUp.postCallPriorities(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'PhoneType') {
        this.dataSubscription = this.lookUp.postPhoneTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'ActionStatus') {
        this.dataSubscription = this.lookUp.postActionStatues(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'ContractType') {
        this.dataSubscription = this.lookUp.postContractTypes(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();

            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'Role') {
        this.dataSubscription = this.lookUp.postRoles(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderType') {
        this.dataSubscription = this.lookUp.postOrderType(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderPriority') {
        this.dataSubscription = this.lookUp.postOrderPriority(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderStatus') {
        this.dataSubscription = this.lookUp.postOrderStatus(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)

          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderProg') {
        console.log(newValue);
        this.dataSubscription = this.lookUp.postOrderProgress(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderProb') {
        console.log(newValue);
        this.dataSubscription = this.lookUp.postOrderProblems(newValue).subscribe((data) => {
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
            this.rows.push(data)
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
    })
      .catch((result) => {
        //console.log('nothing added');
        this.messageService.add({
          severity: this.translate.instant('MSG.info'),
          summary: this.translate.instant('MSG.nothingAd'),
          detail: this.translate.instant('MSG.noSavValu')
        });
      });
  }

  edit(row) {
    //console.log(row);
    console.log(row);
    this.openModal(Object.assign({}, row), this.translate.instant('ADMN.Edit'), this.type);
    this.modalRef.result.then((editedValue) => {
      console.log(editedValue);
      // let editedToPost = {
      //   id: row.id,
      //   name: editedValue.name,
      //   fK_OrderStatus_Id: editedValue.fK_OrderStatus_Id
      // };
      // console.log(editedValue);
      if (this.type.key == 'CustType') {
        this.dataSubscription = this.lookUp.updateCustomerTypes(row).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'CallType') {
        this.dataSubscription = this.lookUp.updateCallsTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'CallPriority') {
        this.dataSubscription = this.lookUp.updatePriorities(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'PhoneType') {
        this.dataSubscription = this.lookUp.updatePhoneTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'ActionStatus') {
        this.dataSubscription = this.lookUp.updateStatues(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'ContractType') {
        this.dataSubscription = this.lookUp.updateContractTypes(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'Role') {
        this.dataSubscription = this.lookUp.updateRoles(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            console.log(row);
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.editSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkUpdtDesc')
            });
          })
      }
      else if (this.type.key == 'OrderType') {
        this.dataSubscription = this.lookUp.updateOrderType(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderPriority') {
        this.dataSubscription = this.lookUp.updateOrderPriority(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderStatus') {
        this.dataSubscription = this.lookUp.updateOrderStatus(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderProg') {
        console.log('OrderProg');
        this.dataSubscription = this.lookUp.updateOrderProgressLookUp(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
      else if (this.type.key == 'OrderProb') {
        console.log('OrderProb');
        this.dataSubscription = this.lookUp.updateOrderProblemsLookUp(editedValue).subscribe((data) => {
            row.name = editedValue.name;
            this.messageService.add({
              severity: this.translate.instant('MSG.suces'),
              summary: this.translate.instant('MSG.sucesful'),
              detail: this.translate.instant('MSG.vluSavSuces')
            });
          },
          err => {
            err.status == 401 && this.utilities.unauthrizedAction();
            this.messageService.add({
              severity: this.translate.instant('MSG.Error_Msg'),
              summary: this.translate.instant('MSG.fail'),
              detail: this.translate.instant('MSG.failNetworkSavDesc')
            });
          })
      }
    })
      .catch((result) => {
        this.messageService.add({
          severity: this.translate.instant('MSG.info'),
          summary: this.translate.instant('MSG.nothingEdit'),
          detail: this.translate.instant('MSG.chngOldValu')
        });
      });
  }

  remove(row) {
    //console.log(row);
    if (this.type.key == 'CustType') {
      this.deleteDataSubscription = this.lookUp.deleteCustomertype(row.id).subscribe(() => {
          // this.rows = data;
          // //console.log(this.rows);
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.custTypRemovSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'CallType') {
      this.dataSubscription = this.lookUp.deleteCallType(row.id).subscribe((data) => {
          // this.rows = data;
          // //console.log(this.rows);
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'CallPriority') {
      this.dataSubscription = this.lookUp.deletePriorities(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'PhoneType') {
      this.dataSubscription = this.lookUp.deletePhoneTypes(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'ActionStatus') {
      this.dataSubscription = this.lookUp.deleteStatus(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'ContractType') {
      this.dataSubscription = this.lookUp.deleteContractType(row.id).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'Role') {
      this.dataSubscription = this.lookUp.deleteRole(row.name).subscribe((data) => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'OrderType') {
      this.dataSubscription = this.lookUp.deleteOrderType(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'OrderPriority') {
      this.dataSubscription = this.lookUp.deleteOrderPriority(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'OrderStatus') {
      this.dataSubscription = this.lookUp.deleteOrderStatus(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'OrderProg') {
      this.dataSubscription = this.lookUp.removeProgressStatus(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
    else if (this.type.key == 'OrderProb') {
      this.dataSubscription = this.lookUp.removeProblem(row.id).subscribe(() => {
          this.rows = this.rows.filter((oneRow) => {
            return oneRow.id != row.id;
          });
          this.messageService.add({
            severity: this.translate.instant('MSG.suces'),
            summary: this.translate.instant('MSG.sucesful'),
            detail: this.translate.instant('MSG.removSuces')
          });
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          this.messageService.add({
            severity: this.translate.instant('MSG.Error_Msg'),
            summary: this.translate.instant('MSG.fail'),
            detail: this.translate.instant('MSG.failNetworkRemovDesc')
          });

        })
    }
  }

  openModal(data, header, type?) {
    debugger;
    console.log('open Modal');
    this.modalRef = this.modalService.open(PromptComponent);
    this.modalRef.componentInstance.header = header;
    this.modalRef.componentInstance.type = type.key;
    this.modalRef.componentInstance.data = data;
  }

}
