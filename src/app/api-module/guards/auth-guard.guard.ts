import {AlertServiceService} from './../services/alertservice/alert-service.service';
import {Injectable} from '@angular/core';
import {Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import {Location} from '@angular/common';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class AuthGuardGuard implements CanActivate {
  public roles: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private router: Router, private _location: Location, private alertService: AlertServiceService) {
    translate.setDefaultLang(this.lang);
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    this.roles = route.data["roles"] as Array<string>;
    console.log(this.roles);
    if (localStorage.getItem('currentUser')) {
      let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
      // let UserAllRoles = ['Admin', 'CallCenter', 'Maintenance'];
      console.log(this.checkRoles(this.roles));
      if (this.checkRoles(this.roles)) {
        return true;
      }
      else {
        if (UserAllRoles.indexOf("Admin") > -1) {
          this.router.navigate(['/admin']);
          return false;
        }
        else if (state.url == '/' && (UserAllRoles.indexOf("Dispatcher") > -1 || UserAllRoles.indexOf("MovementController") > -1 ||UserAllRoles.indexOf("SupervisorDispatcher") > -1)) {
          this.router.navigate(['/dispatcher']);
          return false;
        }
        else {
          this.alertService.error(this.translate.instant("MSG.You_dont_have_permission"));
          return false;

        }

      }
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}});
    return false;
  }

  checkRoles(listRoles: Array<string>) {

    let UserAllRoles = JSON.parse(localStorage.getItem('currentUser')).roles;
    // let UserAllRoles = ['Admin', 'CallCenter', 'Maintenance'];
    //console.log("------------------ UserAllRoles ----------- ");
    //console.log(UserAllRoles);
    //console.log("------------------ listRoles ----------- ");
    //console.log(listRoles);

    for (var i = 0; i < this.roles.length; i++) {

      for (var j = 0; j < UserAllRoles.length; j++) {
        console.log(this.roles)
        console.log(UserAllRoles[j].toLowerCase())
        if (this.roles[i].toLowerCase() == UserAllRoles[j].toLowerCase()) {
          return true;
        }

      }
    }
    return false;
  }
}
