import { CustomerHistory, customer, contract, complain, WorkOrderDetails } from './../../models/customer-model';
import { Observable } from 'rxjs';
import { newCallDetails } from './../../models/newCustomer';
import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import * as myGlobals from '../globalPath';

@Injectable()
export class CustomerCrudService {

  constructor(private http: Http) { }

  GoSearchCustomer(SearchField: string) {
    return this.http.get(myGlobals.SearchCustomer+`${SearchField}`, this.jwt()).map((response: Response) => response.json());
  }
  addCustomer(customer: any) {
    return this.http.post(myGlobals.CreatenewCustomer, customer, this.jwt()).map((response: Response) => response.json());
  }

  GetCallDetail(Id: number): Observable<CustomerHistory> {
    return this.http.get(myGlobals.GetCallDetails + `${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  EditCustomerInfo(customer: customer) {
    return this.http.post(myGlobals.EditCustomerInfo, customer, this.jwt()).map((response: Response) => response.json());
  }
  EditContract(contract: contract) {
    return this.http.post(myGlobals.EditContract, contract, this.jwt()).map((response: Response) => response.json());
  }
  AddContract(contract: contract) {
    return this.http.post(myGlobals.AddContract, contract, this.jwt()).map((response: Response) => response.json());
  }

  EditComplain(complain: complain) {
    return this.http.post(myGlobals.EditComplain, complain, this.jwt()).map((response: Response) => response.json());
  }
  AddComplain(complain: complain) {
    return this.http.post(myGlobals.AddComplain, complain, this.jwt()).map((response: Response) => response.json());
  }

  EditWorkOrderServ(WorkOrderDetails: WorkOrderDetails) {
    return this.http.post(myGlobals.EditWorkOrder, WorkOrderDetails, this.jwt()).map((response: Response) => response.json());
  }
  AddNewWorkOrder(WorkOrderDetails: WorkOrderDetails) {

    return this.http.post(myGlobals.AddNewWorkOrder, WorkOrderDetails, this.jwt()).map((response: Response) => response.json());
  }

  AddNewLocation(location: any) {
    return this.http.post(myGlobals.AddLocation, location, this.jwt()).map((response: Response) => response.json());
  }

  DeleteComplain(Id: any) {
    return this.http.delete(myGlobals.DeleteComplain+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  DeleteWorkOrder(Id: any) {
    return this.http.delete(myGlobals.DeleteWorkOrder+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  DeleteContract(Id: any) {
    return this.http.delete(myGlobals.DeleteContract+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllWorkOrdersDispature(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrdersDispature, this.jwt()).map((response: Response) => response.json());
  }
  GetAllWorkOrdersMovementController(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrdersMovementController, this.jwt()).map((response: Response) => response.json());
  }
  GetAvailableEquipmentInFactory(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AvailableEquipmentInFactory+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  UpdateAssignmentToWorkOrder(newEquip: any) {
    return this.http.post(myGlobals.UpdateAssignmentToWorkOrder, newEquip, this.jwt()).map((response: Response) => response.json());
  }
  UpdateSubOrder(newOrder: any) {
    return this.http.post(myGlobals.UpdateSubOrder, newOrder, this.jwt()).map((response: Response) => response.json());
  }
  ReassinFactory(workorderId: any,factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.reassinFactory+`${workorderId}`+'&factoryId='+`${factoryId}`, this.jwt()).map((response: Response) => response.json());
  }
  UpdateOrderAssignmentToFactory(workorderId: any,factoryId: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AssignmentToFactory+`${workorderId}`+'&factoryId='+`${factoryId}`, this.jwt()).map((response: Response) => response.json());
  }
  GetJobDetails(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.JobDetails+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAvailableVehicleInFactory(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AvailableVehicleInFactory+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }
  AddJop(Job: any) {
    return this.http.post(myGlobals.AddJop, Job, this.jwt()).map((response: Response) => response.json());
  }

  CancelJop(Id: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.CancelJop+`${Id}`, this.jwt()).map((response: Response) => response.json());
  }

  ReassignJop(JobId: any,workorderId:any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.ReassignJop+`${JobId}`+'&workorderId='+`${workorderId}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.AllWorkOrder, this.jwt()).map((response: Response) => response.json());
  }

  checkCustomerNameExist(name: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerNameExist+`${name}`, this.jwt()).map((response: Response) => response.json());
  }

  checkCustomerphoneExist(phone: any): Observable<CustomerHistory> {
    return this.http.get(myGlobals.checkCustomerphoneExist+`${phone}`, this.jwt()).map((response: Response) => response.json());
  }

  GetAllNotificationsWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.NotificationOrders, this.jwt()).map((response: Response) => response.json());
  }

  GetAllRemindWorkOrder(): Observable<CustomerHistory> {
    return this.http.get(myGlobals.RemindOrders, this.jwt()).map((response: Response) => response.json());
  }

  // private helper methods

  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    //console.log(currentUser);
    if (currentUser && currentUser.token.accessToken) {
      let headers = new Headers({ 'Authorization': 'Bearer ' + currentUser.token.accessToken });
      headers.append('Content-Type', 'application/json');
      return new RequestOptions({ headers: headers });
    }
  }
}
