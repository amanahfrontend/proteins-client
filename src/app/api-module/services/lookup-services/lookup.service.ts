// import {allCustomerLookup} from './../../models/lookups-modal';
import { Injectable } from "@angular/core";
import { Http, Headers, RequestOptions, Response } from "@angular/http";
// import {Observable} from 'rxjs/Observable';
import * as myGlobals from "../globalPath";
import { UtilitiesService } from "../utilities/utilities.service";
import { Subject, Observable } from "rxjs";

@Injectable()
export class LookupService {
  orderPostData: any;
  messageSubject = new Subject<any>();

  constructor(private http: Http, private utilities: UtilitiesService) {}
  /*new services Mohamed Ragab*/

  /*GET*/

  getAllPreventive() {
    return this.http
      .get(myGlobals.allPreventive, this.jwt())
      .map((response: Response) => response.json());
  }

  getPriorities() {
    return this.http
      .get(myGlobals.callPriorities, this.jwt())
      .map((response: Response) => response.json());
  }

  getPhoneTypes() {
    return this.http
      .get(myGlobals.phoneTypes, this.jwt())
      .map((response: Response) => response.json());
  }

  getCustomerTypes() {
    return this.http
      .get(myGlobals.customerTypes, this.jwt())
      .map((response: Response) => response.json());
  }

  getCallsHistory() {
    return this.http
      .get(myGlobals.callsHistory, this.jwt())
      .map((response: Response) => response.json());
  }

  getCallsTypes() {
    return this.http
      .get(myGlobals.callsTypes, this.jwt())
      .map((response: Response) => response.json());
  }

  getCallLogs(id) {
    return this.http
      .get(myGlobals.callsLogs + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getCustomerCall(id) {
    return this.http
      .get(myGlobals.GetCustomerDetailsByID + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getStatues() {
    return this.http
      .get(myGlobals.status, this.jwt())
      .map((response: Response) => response.json());
  }

  getItem(code) {
    return this.http
      .get(myGlobals.item + code, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllEstimations() {
    return this.http
      .get(myGlobals.estimations, this.jwt())
      .map((response: Response) => response.json());
  }

  searchByNameOrPhone(searchPhrase) {
    return this.http
      .get(myGlobals.searchByPhoneOrName + searchPhrase, this.jwt())
      .map((response: Response) => response.json());
  }

  getEstimationDetails(id) {
    return this.http
      .get(myGlobals.estimationDetails + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllQuotations() {
    return this.http
      .get(myGlobals.quotations, this.jwt())
      .map((response: Response) => response.json());
  }

  getQuoutationByRefernceNo(searchText) {
    return this.http
      .get(myGlobals.searchQuotation + searchText, this.jwt())
      .map((response: Response) => response.json());
  }

  getQuotationDetailsById(id) {
    return this.http
      .get(myGlobals.quotationbyId + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getQuotationDetailsByRefNo(refNo) {
    return this.http
      .get(myGlobals.quotationbyRefNo + refNo, this.jwt())
      .map((response: Response) => response.json());
  }

  getContractTypes() {
    return this.http
      .get(myGlobals.contractTypes, this.jwt())
      .map((response: Response) => response.json());
  }

  searchContract(refNo) {
    return this.http
      .get(myGlobals.searchContract + refNo, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllContracts() {
    return this.http
      .get(myGlobals.allContracts, this.jwt())
      .map((response: Response) => response.json());
  }

  getContractById(id) {
    return this.http
      .get(myGlobals.contractById + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getRoles() {
    return this.http
      .get(myGlobals.roles, this.jwt())
      .map((response: Response) => response.json());
  }

  getUsers() {
    return this.http
      .get(myGlobals.users, this.jwt())
      .map((response: Response) => response.json());
  }

  getOrderType() {
    return this.http
      .get(myGlobals.orderTypes, this.jwt())
      .map((response: Response) => response.json());
  }

  getOrderPriority() {
    return this.http
      .get(myGlobals.orderPriorities, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllOrders() {
    return this.http
      .get(myGlobals.allOrders, this.jwt())
      .map((response: Response) => response.json());
  }

  searchByOrderNumber(number) {
    return this.http
      .get(myGlobals.searchOrders + number, this.jwt())
      .map((response: Response) => response.json());
  }

  getOrderStatus() {
    return this.http
      .get(myGlobals.orderStatus, this.jwt())
      .map((response: Response) => response.json());
  }

  getContractByCustomerId(id) {
    return this.http
      .get(myGlobals.contractByCustomer + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getOrderByCustomerId(id) {
    return this.http
      .get(myGlobals.orderByCustomer + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getCallByCustomerId(id) {
    return this.http
      .get(myGlobals.callrByCustomer + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getLocationByCustomerId(id) {
    return this.http
      .get(myGlobals.locationByCustomer + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getDispatcherOrders(id) {
    return this.http
      .get(myGlobals.ordersByDis + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getDispatchers() {
    return this.http
      .get(myGlobals.allDispatchers, this.jwt())
      .map((response: Response) => response.json());
  }

  getTecs() {
    return this.http
      .get(myGlobals.unAssignedTecs, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllTecs() {
    return this.http
      .get(myGlobals.allTecs, this.jwt())
      .map((response: Response) => response.json());
  }

  getTecsByDis(id) {
    return this.http
      .get(myGlobals.assignedTecs + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getOrderProgress(id) {
    return this.http
      .get(myGlobals.orderProgressUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getProgressStatuses() {
    return this.http
      .get(myGlobals.progressStatuses, this.jwt())
      .map((response: Response) => response.json());
  }

  getDispatchersWithProb() {
    return this.http
      .get(myGlobals.getDiByProb, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllProblems() {
    return this.http
      .get(myGlobals.allProbs, this.jwt())
      .map((response: Response) => response.json());
  }

  getDispatcherAllProblems(id) {
    return this.http
      .get(myGlobals.getDisWithAllProbs + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllVehicles() {
    return this.http
      .get(myGlobals.getAllVehicles, this.jwt())
      .map((response: Response) => response.json());
  }

  getDisAllOrders(id) {
    return this.http
      .get(myGlobals.getOrdersByDis + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getTecAssignedItems(id) {
    return this.http
      .get(myGlobals.assignedItems + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getTecReportUsedItems(id) {
    return this.http
      .get(myGlobals.usedItems + id, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllCustomers() {
    return this.http
      .get(myGlobals.allCustomers, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllItems() {
    return this.http
      .get(myGlobals.getAllItems, this.jwt())
      .map((response: Response) => response.json());
  }

  downloadQuotationWord(id) {
    return this.http
      .get(myGlobals.quotationWordUrl + id, this.jwt())
      .map((response: Response) => response);
  }

  getAllOrderProgress() {
    return this.http
      .get(myGlobals.allOrderProgressUrl, this.jwt())
      .map((response: Response) => response.json());
  }

  getAllOrderProblems() {
    return this.http
      .get(myGlobals.allOrderProblemsUrl, this.jwt())
      .map((response: Response) => response.json());
  }

  getCustomerDetails(customerId) {
    return this.http
      .get(myGlobals.getCustomerDetails + customerId, this.jwt())
      .map((response: Response) => response.json());
  }

  // downloadQuotationWord(id) {
  //   console.log(myGlobals.quotationWordUrl + id);
  //   return this.http.get(myGlobals.quotationWordUrl + id, this.jwt()).map((response: Response) => response.json());
  // }

  /*POST*/

  postNewLog(newLog) {
    return this.http
      .post(myGlobals.logPostUrl, newLog, this.jwt())
      .map((response: Response) => response.json());
  }

  postEstimation(estimations) {
    return this.http
      .post(myGlobals.estimationPostUrl, estimations, this.jwt())
      .map((response: Response) => response.json());
  }

  postNewCall(call) {
    return this.http
      .post(myGlobals.callPostUrl, call, this.jwt())
      .map((response: Response) => response.json());
  }

  generateQuotation(estimation) {
    return this.http
      .post(myGlobals.generateQuotationUrl, estimation, this.jwt())
      .map((response: Response) => response.json());
  }

  postNewContract(newContract) {
    return this.http
      .post(myGlobals.generateContractUrl, newContract, this.jwt())
      .map((response: Response) => response.json());
  }

  postNewUser(newUser) {
    return this.http
      .post(myGlobals.newUserUrl, newUser, this.jwt())
      .map((response: Response) => response.json());
  }

  postItemsExcelSheet(excelSheetObject) {
    return this.http
      .post(myGlobals.itemsExcelSheetUrl, excelSheetObject, this.jwt())
      .map((response: Response) => response.json());
  }

  postSalaryExcelSheet(excelSheetObject) {
    return this.http
      .post(myGlobals.salaryExcelSheetUrl, excelSheetObject, this.jwt())
      .map((response: Response) => response.json());
  }

  postNewOrder(newOrder) {
    return this.http
      .post(myGlobals.postNewOrderUrl, newOrder, this.jwt())
      .map((response: Response) => response.json());
  }

  postCustomerTypes(customerType) {
    return this.http
      .post(myGlobals.postCustomerTypesUrl, customerType, this.jwt())
      .map((response: Response) => response.json());
  }

  postCallsTypes(callType) {
    return this.http
      .post(myGlobals.postCallsTypesUrl, callType, this.jwt())
      .map((response: Response) => response.json());
  }

  postCallPriorities(callPriority) {
    return this.http
      .post(myGlobals.postCallPrioritiesUrl, callPriority, this.jwt())
      .map((response: Response) => response.json());
  }

  postPhoneTypes(phoneType) {
    return this.http
      .post(myGlobals.postPhoneTypesUrl, phoneType, this.jwt())
      .map((response: Response) => response.json());
  }

  postActionStatues(status) {
    return this.http
      .post(myGlobals.postActionStateUrl, status, this.jwt())
      .map((response: Response) => response.json());
  }

  postContractTypes(contractType) {
    return this.http
      .post(myGlobals.postContractTypeUrl, contractType, this.jwt())
      .map((response: Response) => response.json());
  }

  postRoles(role) {
    return this.http
      .post(myGlobals.postRoleUrl, role, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderType(orderType) {
    return this.http
      .post(myGlobals.postOrderTypeUrl, orderType, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderPriority(orderPriority) {
    return this.http
      .post(myGlobals.postOrderPriorityUrl, orderPriority, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderStatus(orderState) {
    return this.http
      .post(myGlobals.postOrderStateUrl, orderState, this.jwt())
      .map((response: Response) => response.json());
  }

  postTecnichanWithDis(tecWithDis) {
    return this.http
      .post(myGlobals.postTecForDist, tecWithDis, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderToTec(orderByTec) {
    return this.http
      .post(myGlobals.orderByTec, orderByTec, this.jwt())
      .map((response: Response) => response.json());
  }

  postUnAssignedOrder(id) {
    return this.http
      .get(myGlobals.unAssignedOrder + id, this.jwt())
      .map((response: Response) => response.json());
  }

  postTecAvailability(availability) {
    return this.http
      .post(myGlobals.tecAvailable, availability, this.jwt())
      .map((response: Response) => response.json());
  }

  postVisitTime(date) {
    return this.http
      .post(myGlobals.postPreferedVisitDate, date, this.jwt())
      .map((response: Response) => response.json());
  }

  postTransferOrder(transferToDispatcher) {
    return this.http
      .post(myGlobals.transferToDispatcherUrl, transferToDispatcher, this.jwt())
      .map((response: Response) => response.json());
  }

  postProgress(progress) {
    return this.http
      .post(myGlobals.postProgressUrl, progress, this.jwt())
      .map((response: Response) => response.json());
  }

  postDispatcherWithProblemsAndAreas(DisWithProb) {
    return this.http
      .post(myGlobals.postDispatcherWithProblemsUrl, DisWithProb, this.jwt())
      .map((response: Response) => response.json());
  }

  filterOrders(filterObj) {
    return this.http
      .post(myGlobals.filterOrdersUrl, filterObj, this.jwt())
      .map((response: Response) => response.json());
  }

  filterOrdersMap(filterObj) {
    return this.http
      .post(myGlobals.filterOrdersMapUrl, filterObj, this.jwt())
      .map((response: Response) => response.json());
  }

  assignItemToTec(obj) {
    return this.http
      .post(myGlobals.assignItemUrl, obj, this.jwt())
      .map((response: Response) => response.json());
  }

  postCustomer(obj) {
    return this.http
      .post(myGlobals.postCustomerUrl, obj, this.jwt())
      .map((response: Response) => response.json());
  }


 updateCaller(obj) {
   return this.http
     .put(myGlobals.callUpdateUrl, obj, this.jwt())
      .map((response: Response) => response.json());
  }

  assignOrdersToDispatcher(obj) {
    return this.http
      .post(myGlobals.assignOrdersToDispatcherUrl, obj, this.jwt())
      .map((response: Response) => response.json());
  }

  assignOrdersToTec(obj) {
    return this.http
      .post(myGlobals.assignOrdersToTecUrl, obj, this.jwt())
      .map((response: Response) => response.json());
  }

  searchAssignedItems(searchObject) {
    return this.http
      .post(myGlobals.assignedItemSearch, searchObject, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderProgress(objectProgress) {
    return this.http
      .post(myGlobals.postOrderProgress, objectProgress, this.jwt())
      .map((response: Response) => response.json());
  }

  postOrderProblems(objectProblems) {
    return this.http
      .post(myGlobals.postOrderProblems, objectProblems, this.jwt())
      .map((response: Response) => response.json());
  }

  filterUsedItems(filterObject) {
    return this.http
      .post(myGlobals.filterUsedItems, filterObject, this.jwt())
      .map((response: Response) => response.json());
  }

  filterPreventive(filterObject) {
    return this.http
      .post(myGlobals.filterPreventiveUrl, filterObject, this.jwt())
      .map((response: Response) => response.json());
  }

  filterEstimations(filterObject) {
    return this.http
      .post(myGlobals.filterEstimationsUrl, filterObject, this.jwt())
      .map((response: Response) => response.json());
  }

  filterQuotations(filterObject) {
    return this.http
      .post(myGlobals.filterQuotationsUrl, filterObject, this.jwt())
      .map((response: Response) => response.json());
  }

  postNewLocation(location) {
    return this.http
      .post(myGlobals.postNewCustomerLocationUrl, location, this.jwt())
      .map((response: Response) => response.json());
  }

  /*DELETE*/

  deleteEstimation(id) {
    return this.http
      .delete(myGlobals.deletEstimationUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteQuotation(id) {
    return this.http
      .delete(myGlobals.deleteQuotationUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteContract(id) {
    return this.http
      .delete(myGlobals.deleteContractUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteCustomertype(id) {
    return this.http
      .delete(myGlobals.deleteCustomertypeUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteCallType(id) {
    return this.http
      .delete(myGlobals.deleteCallTypeUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deletePriorities(id) {
    return this.http
      .delete(myGlobals.deletePrioritiesUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deletePhoneTypes(id) {
    return this.http
      .delete(myGlobals.deletePhoneTypeUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteStatus(id) {
    return this.http
      .delete(myGlobals.deleteStatusUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteContractType(id) {
    return this.http
      .delete(myGlobals.deleteContractTypeUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteRole(id) {
    return this.http
      .delete(myGlobals.deleteRoleUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteUser(id) {
    return this.http
      .delete(myGlobals.deleteUserUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteOrder(id) {
    return this.http
      .delete(myGlobals.deleteOrderUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteOrderType(id) {
    return this.http
      .delete(myGlobals.deleteOrderTypeUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteOrderPriority(id) {
    return this.http
      .delete(myGlobals.deleteOrderPriorityUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  deleteOrderStatus(id) {
    return this.http
      .delete(myGlobals.deleteOrderStatusUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  removeTecFromDis(id) {
    return this.http
      .delete(myGlobals.deleteTecFromDis + id, this.jwt())
      .map((response: Response) => response.json());
  }

  removeDisByProb(id) {
    return this.http
      .delete(myGlobals.removeDisWithProb + id, this.jwt())
      .map((response: Response) => response.json());
  }

  removeProgressStatus(id) {
    return this.http
      .delete(myGlobals.removeProgressStatusUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  removeProblem(id) {
    return this.http
      .delete(myGlobals.removeProblemUrl + id, this.jwt())
      .map((response: Response) => response.json());
  }

  /*UPDATE*/

  updateEstimation(estimation) {
    return this.http
      .put(myGlobals.updateEstimationUrl, estimation, this.jwt())
      .map((response: Response) => response.json());
  }

  updateCustomerTypes(customerType) {
    return this.http
      .put(myGlobals.updateCustomerTypeUrl, customerType, this.jwt())
      .map((response: Response) => response.json());
  }

  updateCallsTypes(callType) {
    return this.http
      .put(myGlobals.updateCallTypeUrl, callType, this.jwt())
      .map((response: Response) => response.json());
  }

  updatePriorities(priority) {
    return this.http
      .put(myGlobals.updatePriorityUrl, priority, this.jwt())
      .map((response: Response) => response.json());
  }

  updatePhoneTypes(phoneType) {
    return this.http
      .put(myGlobals.updatePhoneTypeUrl, phoneType, this.jwt())
      .map((response: Response) => response.json());
  }

  updateStatues(status) {
    return this.http
      .put(myGlobals.updateStatusUrl, status, this.jwt())
      .map((response: Response) => response.json());
  }

  updateContractTypes(contractType) {
    return this.http
      .put(myGlobals.updateContractTypeUrl, contractType, this.jwt())
      .map((response: Response) => response.json());
  }

  updateRoles(role) {
    return this.http
      .post(myGlobals.updateRoleUrl, role, this.jwt())
      .map((response: Response) => response.json());
  }

  updateUser(userData) {
    return this.http
      .post(myGlobals.updateUserUrl, userData, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrder(order) {
    return this.http
      .put(myGlobals.updateOrderUrl, order, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrderStatus(status) {
    return this.http
      .put(myGlobals.updateOrderStatusUrl, status, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrderPriority(priority) {
    return this.http
      .put(myGlobals.updateOrderPriorityUrl, priority, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrderType(type) {
    return this.http
      .put(myGlobals.updateOrderTypeUrl, type, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrderProgressLookUp(progress) {
    return this.http
      .put(myGlobals.updateOrderProgressUrl, progress, this.jwt())
      .map((response: Response) => response.json());
  }

  updateOrderProblemsLookUp(problem) {
    return this.http
      .put(myGlobals.updateOrderProblemsUrl, problem, this.jwt())
      .map((response: Response) => response.json());
  }

  updateCustomer(customerData) {
    return this.http
      .put(myGlobals.updateCustomerUrl, customerData, this.jwt())
      .map((response: Response) => response.json());
  }

  updateLocation(location) {
    return this.http
      .post(myGlobals.updateLocationUrl, location, this.jwt())
      .map((response: Response) => response.json());
  }

  /*-----------------------------------------------*/

  GetallGovernorates() {
    return this.http
      .get(myGlobals.allGovernorates, this.jwt())
      .map((response: Response) => response.json());
  }

  GetallAreas(Gov: number) {
    return this.http
      .get(myGlobals.allAreas + "?govId=" + Gov, this.jwt())
      .map((response: Response) => response.json());
  }

  GetallBlocks(area: number) {
    return this.http
      .get(myGlobals.allBlocks + "?areaId=" + area, this.jwt())
      .map((response: Response) => response.json());
  }

  GetallStreets(area: number, block: string, gov: number) {
    return this.http
      .get(
        myGlobals.allStreets +
          "?govId=" +
          gov +
          "&areaId=" +
          area +
          "&blockName=" +
          block,
        this.jwt()
      )
      .map((response: Response) => response.json());
  }

  GetLocationByPaci(PACI: number) {
    return this.http
      .get(myGlobals.allGetLocationByPaci + PACI, this.jwt())
      .map((response: Response) => response.json());
  }

  // private helper methods
  private jwt() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    if (currentUser && currentUser.token.accessToken) {
      let headers = new Headers({
        Authorization: "bearer " + currentUser.token.accessToken
      });
      headers.append("Content-Type", "application/json");
      return new RequestOptions({ headers: headers });
    }
  }

  // private jwtDoc() {
  //   // create authorization header with jwt token
  //   let currentUser = JSON.parse(localStorage.getItem('currentUser'));
  //   if (currentUser && currentUser.token.accessToken) {
  //     let headers = new Headers({'Authorization': 'bearer ' + currentUser.token.accessToken});
  //     headers.append('Content-Type', 'application/json');
  //     return new RequestOptions({headers: headers});
  //   }
  // }

  private header() {
    // create authorization header with jwt token
    let currentUser = JSON.parse(localStorage.getItem("currentUser"));
    if (currentUser && currentUser.token.accessToken) {
      var headers = new Headers({
        Authorization: "bearer " + currentUser.token.accessToken
      });
      headers.append("Content-Type", "application/json");
      return headers;
    }
  }
}
