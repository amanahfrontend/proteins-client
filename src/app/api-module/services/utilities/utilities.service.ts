import {Injectable} from '@angular/core';
import {BehaviorSubject} from "rxjs";
import {NavigationEnd, Router} from "@angular/router";
import {TranslateService} from '@ngx-translate/core';
import * as $ from 'jquery';

// import {Subject} from "rxjs/Subject";

@Injectable()
export class UtilitiesService {
  existedCustomer = new BehaviorSubject(<any>{});
  routingData = new BehaviorSubject(<any>{});
  toggleFullWidth: boolean;
  currentSearch: any;
  // currentSearchQuotation: any;
  // currentSearchContract: any;
  // currentNotificationOrder: any;
  routingFromAndHaveSearch: boolean;
  savedNotificationText = new BehaviorSubject(<any>'');
  previousUrl: any;
  statusColors: any =
    [
      {
        name: 'open',
        color: 'rgb(36, 15, 255, 1)'
      },
      {
        name: 'dispatched',
        color: 'rgb(255, 118, 0)'
      },
      {
        name: 'in progress',
        color: 'rgb(255, 234, 0)'
      },
      {
        name: 'completed',
        color: ' rgb(12, 118, 20)'
      },
      {
        name: 'cancelled',
        color: 'rgb(144, 139, 138)'
      }
    ];

  constructor(private router: Router ,private translate: TranslateService) {
    this.router.events
      .filter(event => event instanceof NavigationEnd)
      .subscribe(e => {
        console.log('prev:', this.previousUrl);
        this.previousUrl = e['url'];
      });

       this.translate.setDefaultLang('en');
      this.translate.use('en');
  }

  setSavedNotificationText(value) {
    this.savedNotificationText.next(value);
  }

  updateCurrentExistedCustomer(newExistedCustomer) {
    this.existedCustomer.next(newExistedCustomer);
  }

  unauthrizedAction() {
    this.router.navigate(['/login']);
  }
 languages = [
      {id: 1, name: 'english', symbol: 'en'},
      {id: 2, name: 'arabic', symbol: 'ar'}
    ];

  setRoutingDataPassed(data) {
    this.routingData.next(data);
  }

  hideGrowl(time) {
    return setTimeout(() => {
      return [];
    }, time)
  }

  convertDatetoNormal(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }


    convertDateForSaving(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${month}/${day}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  getNextMonthForCurrentDate(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 2;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${day}/${month}/${year}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  convertDateForSearchBinding(date) {
    date = new Date(`${date}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let day = date.getDate();

    if (day < 10) {
      day = '0' + day;
    }
    if (month < 10) {
      month = '0' + month;
    }
    let formatedDate = `${year}-${month}-${day}`;
    //console.log(formatedDate);
    return formatedDate;
  }

  printComponent(elementId) {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=1000px,width=1000px');

    var divToPrint = document.getElementById(elementId);
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

   chooseLang(lang) {
    this.translate.use(lang);
    window.localStorage.setItem('lang', lang);
    if (lang == 'ar') {
      // localStorage.setItem('lang', 'ar');
      // this.translate.use(lang);
      $('#theme-stylesheet').after('<link id="styleAr" rel="stylesheet" href="./assets/stylesheets/styleAr.css" type="text/css" />');
    }
    else {
      // localStorage.setItem('lang', 'en');
      // this.translate.use(lang);
      $('#styleAr').remove()
    }
  }

  setOrdersColorByState(orders) {
    return orders.map((order) => {
      let orderStatus = (order.status && order.status.name.toLowerCase()) || (order.orderStatus && order.orderStatus.name.toLowerCase());
      return this.statusColors.map((color) => {
        if (color.name == orderStatus) {
          return order.color = color.color;
        }
      });
    });
  }
}
