import {
  Component, OnInit, DoCheck
} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AuthenticationServicesService} from "./api-module/services/authentication/authentication-services.service";
import {UtilitiesService} from "./api-module/services/utilities/utilities.service";
// import {HubConnection} from "@aspnet/signalr-client";
// import {quotationHubUrl} from '../app/api-module/services/globalPath';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit, DoCheck {
  title = 'app';
  menuItemToggle: boolean;
  roles: string[];
  toggleFullWidth: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private authService: AuthenticationServicesService, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
   
  }

  ngDoCheck() {
    this.menuItemToggle = this.authService.CurrentUser();
    this.toggleFullWidth = this.utilities.toggleFullWidth;
    // console.log(!this.hubInited);
  }

  ngOnInit() {
    this.authService.activateSignalR();
    this.roles = [];
    this.authService.userRoles.subscribe((roles) => {
      this.roles = roles || [];

    });
  }

  openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginRight = "250px";
  }

  closeNav() {
    console.log('nav closed');

    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginRight = "0";
  }

}
