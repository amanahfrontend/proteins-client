import { HashLocationStrategy, LocationStrategy } from "@angular/common";
import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";
import { BaseRequestOptions, HttpModule } from "@angular/http";
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MessageService } from "primeng/components/common/messageservice";
import { AuthenticationServicesService } from "./api-module/services/authentication/authentication-services.service";
import { UtilitiesService } from "./api-module/services/utilities/utilities.service";
import { AppComponent } from "./app.component";
import { RoutingComponents, RoutingModule } from "./router.module";
import { SharedModuleModule } from "./shared-module/shared-module.module";
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
@NgModule({
  declarations: [
    AppComponent,
    RoutingComponents
  ],

  imports: [
    BrowserModule,
    HttpClientModule,

    BrowserAnimationsModule,
        TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),

    RoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    // SpinnerModule,
    SharedModuleModule.forRoot()
    // SignalRModule.forRoot(createConfig)
  ],

  providers: [
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    BaseRequestOptions, UtilitiesService, MessageService, AuthenticationServicesService,
    HttpClient
  ],

  bootstrap: [AppComponent]
})
export class AppModule {
}
