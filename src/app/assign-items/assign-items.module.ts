import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AssignItemsComponent} from './assign-items/assign-items.component';
import {Routes, RouterModule} from "@angular/router";
import {AuthGuardGuard} from "../api-module/guards/auth-guard.guard";
import {ProgressSpinnerModule} from 'primeng/components/progressspinner/progressspinner';
import {FormsModule} from '@angular/forms';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {ReleaseModalComponent} from './release-modal/release-modal.component';
import {SharedModuleModule} from "../shared-module/shared-module.module";
import {QRCodeModule} from 'angular2-qrcode';
import {GrowlModule} from 'primeng/components/growl/growl';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from "@angular/common/http";
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}


const assignItemsRoutes: Routes = [
  {
    path: '',
    component: AssignItemsComponent,
    canActivate: [AuthGuardGuard],
    data: {roles: ['Dispatcher', 'Admin','SupervisorDispatcher']}
  }
];

@NgModule({
  imports: [
    GrowlModule,
    CommonModule,
    FormsModule,
    SharedModuleModule,
    NgxDatatableModule,
    ProgressSpinnerModule,
    QRCodeModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forChild(assignItemsRoutes)
  ],
  entryComponents: [
    ReleaseModalComponent
  ],
  declarations: [AssignItemsComponent, ReleaseModalComponent]
})
export class AssignItemsModule {
}
