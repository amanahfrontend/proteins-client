import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ReleaseModalComponent } from "../release-modal/release-modal.component";
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Subscription } from "rxjs";
import { Message } from "primeng/components/common/message";
import { AuthenticationServicesService } from "../../api-module/services/authentication/authentication-services.service";
import { SearchComponent } from "../../shared-module/search/search.component";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-assign-items',
  templateUrl: './assign-items.component.html',
  styleUrls: ['assign-items.component.css']
})

export class AssignItemsComponent implements OnInit, OnDestroy {
  tecItems: any[];
  tecReport: any[];
  inventoryItems: any[];
  allItemsSubscription: Subscription;
  tecItemsSubscription: Subscription;
  reportSubscription: Subscription;
  tecs: any[];
  tec: any;
  searchTecObj: any = {};
  toggleLoading: boolean;
  toggleSearch: boolean;
  datesRequired: boolean;
  toggleFilterLoading: boolean;
  formSubmitted: boolean = false;
  activeTab: number;
  cornerMessage: Message[] = [];
  @ViewChild(SearchComponent) searchComponent: SearchComponent;
  lang = localStorage.getItem('lang');
  add:string;
  release:string;

  constructor(private translate: TranslateService,private lookup: LookupService, private modalService: NgbModal, private authService: AuthenticationServicesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.tecItems = [];
    this.tecReport = [];
    this.inventoryItems = [];
    this.getAllTecs();
    this.toggleLoading = true;
    this.datesRequired = false;
    this.activeTab = 1;
    if(this.lang == "ar")
    {
      this.add = "اضافه";
      this.release="ازاله";
    }
     else if (this.lang == "en")
     {
      this.add = "Add";
      this.release="Release";
     }
  }

  ngOnDestroy() {
    this.allItemsSubscription && this.allItemsSubscription.unsubscribe();
    this.tecItemsSubscription && this.tecItemsSubscription.unsubscribe();
    this.reportSubscription && this.reportSubscription.unsubscribe();
  }

  searchItem(searchTerm) {
    console.log(searchTerm);
    this.toggleLoading = true;
    let searchObject = {
      itemId: searchTerm,
      fK_Technican_Id: this.searchTecObj.tec
    };
    console.log(searchObject);
    this.lookup.searchAssignedItems(searchObject).subscribe((item) => {
      console.log(item);
      this.searchComponent.searchText = '';
      this.toggleLoading = false;
      if (item === true) {
        this.cornerMessage.push({
          severity: this.translate.instant("MSG.info"),
          summary: this.translate.instant("MSG.Item_Already_Assigned"),
          detail: this.translate.instant("MSG.Item_Already_Assigned_to_Technician")
        })
      }
      else if (!item) {
        this.cornerMessage.push({
          severity: this.translate.instant("MSG.Error_Msg"),
          summary: this.translate.instant("MSG.Item_Not_Found"),
          detail: this.translate.instant("MSG.This_Item_Not_found_in_inventory")
        })
      }
      else if (item.id) {
        this.tecItems.unshift({
          id: item.id,
          totalUsed: 0,
          totalAmount: 0,
          currentAmount: 0,
          item: {
            name: item.name,
            description: item.description
          },
          isNew: true
        });
      }
      console.log(this.tecItems);
    },
      err => {
        this.toggleLoading = false;
      })
  }

  printReport(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("report-print");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  createQRcode(item): void {
    let printContents, popupWin;
    printContents = document.getElementById(`${item.id}`).innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
      <html>
        <head>
          <title>Print tab</title>
          <style>
          //........Customized style.......
          </style>
        </head>
    <body onload="window.print();window.close()">${printContents}</body>
      </html>`
    );
    popupWin.document.close();
  }

  ConvertToString(item) {
    return String(item.id);
  }

  assignStartDate(date) {
    console.log(date);
    this.searchTecObj.startDate = date;
  }

  assignEndDate(date) {
    console.log(date);
    this.searchTecObj.endDate = date;
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Order No': 'Order No',
      'Contract No': 'Contract No',
      'Customer Name': 'Customer Name',
      'Customer Mobile': 'Customer Mobile',
      'Governarate': 'Governarate',
      'Area': 'Area',
      'Item Name': 'Item Name',
      'Item Description': 'Item Description',
      'Quantity': 'Quantity',
    });
    this.tecReport.map((item) => {
      exportData.push({
        'Order No': item.order.code,
        'Contract No': item.order.contract.contractNumber,
        'Customer Name': item.order.customer.name,
        'Customer Mobile': item.order.customer.name,
        'Governarate': item.order.location.governorate,
        'Area': item.order.location.area,
        'Item Name': item.item.name,
        'Item Description': item.item.description,
        'Quantity': item.amount,
      })
    });
    return new Angular2Csv(exportData, 'Used Items Report', {
      showLabels: true
    });
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    console.log(this.tec);
    if (this.tec && this.activeTab == 1) {
      this.getAssignedItems(this.tec);
    } else if (this.tec && this.activeTab == 2) {
      this.getReport(this.tec);
    } else if (this.activeTab == 3) {
      this.getAllItems();
    }
  }

  submitToSearch(value) {
    console.log(value);
    this.toggleFilterLoading = true;
    if (this.activeTab == 1) {
      this.getAssignedItems(value.tec);
      // this.toggleSearch = true;
    } else if (this.activeTab == 2) {
      this.getReport(value.tec);
    }
  }

  releaseItems(item) {
    let releaseModalRef = this.modalService.open(ReleaseModalComponent);
    releaseModalRef.componentInstance.data = {
      fK_Technican_Id: this.searchTecObj.tec,
      itemId: item.itemId || item.id
    };
    releaseModalRef.result
      .then((res) => {
        console.log(res);
        // item = res;
        // item.currentAmount = res.currentAmount;
        // item.totalAmount = res.totalAmount;
        // item.totalUsed = res.totalUsed;
        this.getAssignedItems(this.searchTecObj.tec);
        console.log(item)
      })
      .catch(() => {
        console.log('failed');
      })
  }

  /*API calls*/

  getAllTecs() {
    let dispatcherId = this.authService.CurrentUser().id;
    if (this.authService.CurrentUser().userName == 'dispatcher') {
      this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
        this.toggleLoading = false;
        this.tecs = tecs;
        console.log(tecs)
      },
        err => {
          this.toggleLoading = false;
          console.log('failed');
        })
    }
    else {
      this.lookup.getAllTecs().subscribe(tecs => {
        this.toggleLoading = false;
        this.tecs = tecs;
        console.log('tecs2' + tecs)
      },
        err => {
          this.toggleLoading = false;
          console.log('failed');
        })
    }

  }

  getAllItems() {
    this.allItemsSubscription = this.lookup.getAllItems().subscribe((items) => {
      this.inventoryItems = items;
      console.log(this.inventoryItems);
    },
      err => {

      })
  }

  getAssignedItems(id) {
    // this.toggleLoading = true;
    this.toggleSearch = false;
    this.tecItemsSubscription = this.lookup.getTecAssignedItems(id).subscribe((items) => {
      this.tecItems = items;
      this.toggleLoading = false;
      this.toggleSearch = true;
      this.toggleFilterLoading = false;
      console.log(this.tecItems)
    },
      err => {
        console.log('failed');
        this.toggleSearch = true;
        this.toggleLoading = false;
        this.toggleFilterLoading = false;
      })
  }

  getReport(id) {
    let tecId = [id];
    let filterObj = {
      fK_Technican_Ids: tecId,
      dateFrom: this.searchTecObj.startDate && this.searchTecObj.startDate.toISOString(),
      dateTo: this.searchTecObj.endDate && this.searchTecObj.endDate.toISOString()
    };
    console.log(filterObj);
    this.reportSubscription = this.lookup.filterUsedItems(filterObj).subscribe((tecReport) => {
      this.tecReport = tecReport;
      console.log(this.tecReport);
      this.toggleFilterLoading = false;
    },
      err => {
        console.log('failed');
        this.toggleFilterLoading = false;
      })
  }

}
