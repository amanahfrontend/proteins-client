import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-release-modal',
  templateUrl: './release-modal.component.html',
  styleUrls: ['./release-modal.component.css']
})

export class ReleaseModalComponent implements OnInit {
  @Input() data;
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,public activeModal: NgbActiveModal, private lookup: LookupService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
  }

  confirm(value) {
    console.log(value);
    this.lookup.assignItemToTec(value).subscribe((res) => {
        console.log(res);
        this.activeModal.close(res);
      },
      err => {
        console.log('failed');
      })
  }

  close() {
    this.activeModal.dismiss();
  }


}
