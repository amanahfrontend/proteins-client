import {Component, OnInit, OnDestroy, Input, Output, EventEmitter} from '@angular/core';
import {Subscription} from "rxjs";
import {Message} from "primeng/components/common/message";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Router} from "@angular/router";
import {MessageService} from "primeng/components/common/messageservice";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-add-contract-form',
  templateUrl: './add-contract-form.component.html',
  styleUrls: ['./add-contract-form.component.css']
})

export class AddContractFormComponent implements OnInit, OnDestroy {
  newContract: any;
  // newCustomer: any;
  contractTypes: any[];
  orderTypes: any[];
  orderPriorities: any[];
  // customerTypes: any[];
  problems: any[];
  // governorates: any[];
  // areas: any[];
  // streets: any[];
  // customer: any;
  @Input() quotationDetails: any;
  @Input() type: string;
  @Output() closedForm = new EventEmitter();
  @Output() dismissForm = new EventEmitter();
  postNewContractSubscription: Subscription;
  subscribtionContractTypes: Subscription;
  toggleLoading: boolean;
  toggleLoadingContractSection: boolean;
  dateConvertedFlag: boolean;
  isNewCustomer: boolean;
  customerId: any;
  customers: any[];
  // phoneTypes: any[];
  // blocks: any[];
  orderPrioritySubscription: Subscription;
  orderTypesSubscription: Subscription;
  cornerMessageContract: Message[] = [];
  uploadedFiles: any[] = [];
  contractFiles: any[] = [];
  toggleUploadButtons: boolean;
  lang = localStorage.getItem('lang');

  // itemsError: Message[];

  constructor(private translate: TranslateService,private lookup: LookupService, private messageService: MessageService, private router: Router, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.newContract = {};
    // this.newCustomer = {};
    this.dateConvertedFlag = false;
    this.isNewCustomer = false;
    this.toggleUploadButtons = false;
    this.subscribtionContractTypes = this.lookup.getContractTypes().subscribe((contractTypes) => {
        this.contractTypes = contractTypes;
        //console.log(this.contractTypes);
      },
      err => {
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error, please try again later.'
        })
      });
    this.getOrderPriorities();
    this.getOrderTypes();
    this.getProblems();
    this.getCustomers();
    // this.getCustomerType();
    // this.getPhoneTypes();
    // this.getGovernorates();
  }

  showAtView(event) {
    console.log(event.files);
    this.uploadedFiles = [];
    console.log(event.files.length);
    this.contractFiles = [];
    for (let i = 0, l = event.files.length; i < l; i++) {
      console.log(event.files[i]);
      this.uploadedFiles.push(event.files[i]);

      let reader = new FileReader();
      let base64file;
      let file = event.files[i];
      console.log('before load');
      reader.onload = (readerEvt) => {
        console.log('at load');
        let binaryStringFileReader = <FileReader>readerEvt.target;
        let binaryString = binaryStringFileReader.result;
        base64file = btoa(binaryString);

        /*********** collect the json object *************/

        console.log(file);
        console.log(i);
        let jsonObject = {
          "fileName": file.name,
          "fileContent": base64file
        };
        console.log(jsonObject);
        this.contractFiles.push(jsonObject);
        console.log(this.contractFiles);
      };

      /* Not sure about the importance of this libe :( */
      let x = reader.readAsBinaryString(event.files[i]);
      console.log(x);
    }
    console.log(this.uploadedFiles);
    // console.log(contractFiles);
  }

  ngOnDestroy() {
    this.subscribtionContractTypes.unsubscribe();
    this.postNewContractSubscription && this.postNewContractSubscription.unsubscribe();
    this.orderPrioritySubscription && this.orderPrioritySubscription.unsubscribe();
    this.orderTypesSubscription && this.orderTypesSubscription.unsubscribe();
  }

  checkChoosedCUstomer() {
    console.log(typeof (this.customerId));
    if (this.customerId == 'undefined') {
      this.customerId = undefined;
    }
  }

  setCustomerId(newCustomer) {
    this.customerId = newCustomer.id;
    this.customers.push(newCustomer);
    this.isNewCustomer = false;
  }

  getCustomers() {
    this.lookup.getAllCustomers().subscribe((customers) => {
      this.customers = customers;
    })
  }

  toggleNewCustomerForm() {
    this.isNewCustomer = !this.isNewCustomer;
  }

  getProblems() {
    this.lookup.getAllProblems().subscribe((problems) => {
        this.problems = problems;
      },
      err => {
        console.log('failed');
      })
  }

  getOrderPriorities() {
    this.orderPrioritySubscription = this.lookup.getOrderPriority().subscribe((orderPriorities) => {
        this.orderPriorities = orderPriorities;
      },
      err => {

      })
  }

  getOrderTypes() {
    this.orderTypesSubscription = this.lookup.getOrderType().subscribe((orderTypes) => {
        this.orderTypes = orderTypes;
      },
      err => {

      })
  }

  // closeFormNow() {
  //   this.closedForm.emit();
  // }

  dismissFormNow() {
    this.dismissForm.emit();
  }

  remove(e) {
    console.log(e);
    console.log(e.file.name);
    this.contractFiles = this.contractFiles.filter((file) => {
      console.log(file.fileName);
      return e.file.name !== file.fileName;
    })
  }

  generateContract(contractValues) {
    console.log(contractValues);

    this.toggleLoadingContractSection = true;
    contractValues.startDate = !this.dateConvertedFlag && contractValues.startDate.toISOString();
    contractValues.endDate = !this.dateConvertedFlag && contractValues.endDate.toISOString();
    if (this.quotationDetails) {
      contractValues.contractQuotations = [{
        quotationRefNumber: this.quotationDetails.refNumber
      }];
      contractValues.price = this.quotationDetails.price;
    } else {
      contractValues.fK_Customer_Id = this.customerId;
    }

    contractValues.contractFiles = this.contractFiles;
    // console.log(contractValues);
    this.dateConvertedFlag = true;
    this.postNewContractSubscription = this.lookup.postNewContract(contractValues).subscribe(() => {
        // console.log(contract);
        this.closedForm.emit();
        this.utilities.currentSearch = {};
        console.log(this.utilities.routingFromAndHaveSearch);
        this.cornerMessageContract.push({
          severity: this.translate.instant("MSG.suces"),
          summary: this.translate.instant("MSG.gnretSuces"),
          detail: this.translate.instant("MSG.contGnrtSuces"),
        });
        this.toggleLoadingContractSection = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/contract'])
        }, 2000);
      },
      err => {
        //console.log(err);
        //console.log(JSON.stringify(err));
        this.utilities.currentSearch = {};
        this.cornerMessageContract.push({
          severity: this.translate.instant("MSG.Error_Msg"),
          summary: this.translate.instant("MSG.fail"),
          detail:  this.translate.instant("MSG.Failed_to_generate_Contract"),
        });
        this.toggleLoadingContractSection = false;
      })
  }

}
