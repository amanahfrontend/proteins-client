import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { LookupService } from '../../api-module/services/lookup-services/lookup.service';
import { MessageService } from 'primeng/components/common/messageservice';
import { UtilitiesService } from '../../api-module/services/utilities/utilities.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-customer-from-caller',
  templateUrl: './add-customer-from-caller.component.html',
  styleUrls: ['./add-customer-from-caller.component.css']
})
export class AddCustomerFromCallerComponent implements OnInit {
  callId: number;
  newCustomer: any;
existedCustomerData: any;
    existedCustomerDataSubsciption: Subscription;

  constructor(private activeRoute: ActivatedRoute, private lookUp: LookupService, private utilities: UtilitiesService, private messageService: MessageService) { }

  ngOnInit() {
      this.activeRoute.params.forEach((param: Params) => {
        this.callId = +param['id'];
        this.existedCustomerDataSubsciption=  this.lookUp.getCustomerCall(this.callId).subscribe((call) => {

          this.existedCustomerData = call;
        },
          err => {
            if (err.status == 401) {
              this.messageService.add({
                severity: 'unathorize',
                summary: 'unathorize',
                detail: 'Unautorize'
              });
            } else {
              this.messageService.add({
                severity: 'error',
                summary: 'Server Error',
                detail: 'Failed to load data due to server error'
              });
            }
          })
      })

  }


  
  ngOnDestroy() {
    this.existedCustomerDataSubsciption.unsubscribe();
  }

  }

