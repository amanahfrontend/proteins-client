import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
// import {Message} from "primeng/components/common/message";
// import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import { ActivatedRoute, Params, Router } from "@angular/router";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-add-edit-customer',
  templateUrl: './add-edit-customer.component.html',
  styleUrls: ['./add-edit-customer.component.css']
})
export class AddEditCustomerComponent implements OnInit, OnChanges {
  newCustomer: any;
  customerTypes: any[];
  governorates: any[];
  areas: any[];
  streets: any[];
  phoneTypes: any[];
  blocks: any[];
  cornerMessage: any = [];
  @Input() existedCustomerData: any;
  toggleLoadingLocation: boolean;
  // customerId: any;
  @Output() customerIdValue = new EventEmitter();
  @Output() closeFormNow = new EventEmitter();
  location: any = {};
  phoneData: any = {};
  allLocations: any[] = [];
  phoneBook: any[] = [];
  hideCustomerButtons: boolean = false;
  disableForm: boolean = false;
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private lookup: LookupService, private router: Router) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.getCustomerType();
    this.getPhoneTypes();
    if (this.newCustomer == undefined) {
      this.newCustomer = {
        locations: [{}],
        customerPhoneBook: [{}]
      };
    }
    //this.newCustomer = this.existedCustomerData;
    this.getGovernorates();
  }

  ngOnChanges() {
    console.log(this.existedCustomerData);
    if (this.existedCustomerData) {
      // this.toggleLoadingLocation = true;

      this.newCustomer= JSON.parse(JSON.stringify(this.existedCustomerData));
 
      if (this.newCustomer.hasOwnProperty('callerName')) {
        this.hideCustomerButtons = true;
        this.newCustomer.name = this.existedCustomerData.callerName;
        this.newCustomer.remarks = this.existedCustomerData.customerDescription;

        this.location.paciNumber = this.newCustomer.paciNumber;
        this.location.area = this.newCustomer.area;
        this.location.block = this.newCustomer.block;
        this.location.street = this.newCustomer.street;
        this.location.addressNote = this.newCustomer.addressNote;
        this.location.governorate = this.newCustomer.governorate;
        this.location.latitude = this.newCustomer.latitude;
        this.location.longitude = this.newCustomer.longitude;
        this.phoneData.phone = this.newCustomer.callerNumber;
        this.phoneData.fK_PhoneType_Id = this.newCustomer.fK_PhoneType_Id;
        if (Object.keys(this.location).length > 0) {
          this.allLocations.push(this.location);
          this.newCustomer.locations = this.allLocations;
        }
        if (Object.keys(this.phoneData).length > 0) {
          this.phoneBook.push(this.phoneData);
          this.newCustomer.customerPhoneBook = this.phoneBook;
        }


      }



      this.getMultipleLocations(0, this.newCustomer.locations)
      console.log(this.newCustomer);
    }
  }

  getMultipleLocations(i, locations) {
    console.log(i);
    console.log(locations.length);
    if (i == locations.length) {
      console.log('recursive done!');
    } else {
      this.getGovernorates().then(() => {
        console.log('in gov success promise');
        this.getAreas(this.newCustomer.locations[i]).then(() => {
          console.log('in area success promise');
          this.getBlocks(this.newCustomer.locations[i]).then(() => {
            console.log('in blocks success promise');
            this.getStreets(this.newCustomer.locations[i]).then(() => {
              this.getMultipleLocations(++i, locations)
            })
          })
        })
      });
    }
  }

  addNewLocation() {
    this.newCustomer.locations.push({ isNewLocation: true });
  }

  addNewPhone() {
    this.newCustomer.customerPhoneBook.push({});
    console.log(this.newCustomer.customerPhoneBook);
  }

  removePhone(index) {
    console.log(index);
    this.newCustomer.customerPhoneBook.splice(index, 1);
  }

  saveLocation(loc) {
    console.log(loc);
    let locationToPost = {
      "PACINumber": loc.paciNumber,
      "governorate": loc.governorate,
      "area": loc.area,
      "block": loc.block,
      "street": loc.street,
      "title": loc.title,
      "addressNote": loc.addressNote,
      "fk_Customer_Id": this.existedCustomerData.id,
      "id": loc.id
    };
    console.log(locationToPost);
    if (loc.isNewLocation) {
      this.lookup.postNewLocation(locationToPost).subscribe(() => {
        console.log('success');
        this.cornerMessage.push({
          severity: "success",
          summary: "Successfully!",
          detail: "Location Added Successfully!"
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000)
      },
        err => {
          console.log('failed');
        })
    } else {
      this.lookup.updateLocation(locationToPost).subscribe(() => {
        console.log('success');

        console.log('success');
        this.cornerMessage.push({
          severity: "success",
          summary: "Successfully!",
          detail: "Location Saved Successfully!"
        });
        setTimeout(() => {
          this.cornerMessage = [];
        }, 2000)
      },
        err => {
          console.log('failed');
        })
    }
  }

  saveCustomerInfo(info) {
    console.log(info);
    let customerPhoneBookToPost = [];
    info.customerPhoneBook.map((singlePhoneBook) => {
      customerPhoneBookToPost.push({
        // id: info.id,
        phone: singlePhoneBook.phone,
        fK_PhoneType_Id: singlePhoneBook.fK_PhoneType_Id
      });
    });
    let infoPostObject = {
      id: info.id,
      name: info.name,
      remarks: info.remarks,
      customerPhoneBook: customerPhoneBookToPost,
      fK_CustomerType_Id: info.fK_CustomerType_Id
    };
    console.log(infoPostObject);
    this.lookup.updateCustomer(infoPostObject).subscribe(() => {
      console.log('success');
    },
      err => {
        console.log('failed');
      })
  };

  closeForm() {
    this.closeFormNow.emit();
  }

  closeCusomerForm() {
    this.router.navigate(['search/calls-history'])
  }

  getPhoneTypes() {
    this.lookup.getPhoneTypes().subscribe((phoneTypes) => {
      this.phoneTypes = phoneTypes;
    },
      err => {

      })
  }

  getCustomerType() {
    this.lookup.getCustomerTypes().subscribe((customerTypes) => {
      this.customerTypes = customerTypes;
    },
      err => {

      })
  }

  postNewCustomer() {
        this.disableForm = true;

    let allLocations = [];
    this.newCustomer.locations.map((singleLoc) => {
      allLocations.push({
        // id: info.id,
        addressNote: singleLoc.addressNote,
        area: singleLoc.area,
        block: singleLoc.block,
        governorate: singleLoc.governorate,
        latitude: singleLoc.latitude,
        paciNumber: singleLoc.paciNumber,
        longitude: singleLoc.longitude,
        street: singleLoc.street,
        title: singleLoc.title,
      });
    });


    let infoPostObject = {
      name: this.newCustomer.name,
      remarks: this.newCustomer.remarks,
      customerPhoneBook: this.newCustomer.customerPhoneBook,
      fK_CustomerType_Id: this.newCustomer.fK_CustomerType_Id,
      locations: allLocations
    };

    if (this.newCustomer.hasOwnProperty('callerName')) {

      this.lookup.postCustomer(infoPostObject).subscribe((res) => {

        this.updateCaller(res);

      },
        err => {
          console.log('failed');
        });
    }

    else {
      this.lookup.postCustomer(this.newCustomer).subscribe((res) => {
        // this.customerId = res;
        this.customerIdValue.emit(res);
        console.log('success');
      },
        err => {
          console.log('failed');
        });
    }


  }
  updateCaller(caller: any): any {
    this.existedCustomerData.fK_Customer_Id = caller.id;
    this.existedCustomerData.fK_CustomerType_Id = this.newCustomer.fK_CustomerType_Id;
    this.lookup.updateCaller( this.existedCustomerData).subscribe((res) => {
      this.router.navigate(['search/customersList'])
      console.log('success');
    },
      err => {
        console.log('failed');
      });
  }

  getGovernorates() {
    return new Promise((resolve, reject) => {
      // (function internalRecursive() {
      // }());
      this.lookup.GetallGovernorates().subscribe((govs) => {
        if (govs[0].name == 'Not Found In Paci') {
          console.log('will retry');
          this.getGovernorates();
        } else {
          console.log('got governorates');
          this.governorates = govs;
          resolve();
        }
      },
        err => {
          console.log(err);
        })
    })
  }

  getAreas(singleLocation) {
    console.log('in getting areas');
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.governorate, this.governorates);
      console.log(id);
      var lookup = this.lookup;
      (function internalRecursive() {
        lookup.GetallAreas(id).subscribe((areas) => {
          if (areas[0].name == 'Not Found In Paci') {
            console.log('will retry Areas');
            internalRecursive();
          } else {
            singleLocation.areas = areas;
            resolve();
          }
        })
      }());
    })
  }

  getBlocks(singleLocation) {
    return new Promise((resolve, reject) => {
      let id = this.selectId(singleLocation.area, singleLocation.areas);
      console.log(id);
      var lookUp = this.lookup;
      (function internalRecursive() {
        lookUp.GetallBlocks(id).subscribe((blocks) => {
          if (blocks[0].name == 'Not Found In Paci') {
            console.log('will retry blocks');
            internalRecursive();
          } else {
            singleLocation.blocks = blocks;
            resolve();
          }
        },
          err => {
            console.log('failed');
          })
      })();

    })
  }

  getStreets(singleLocation) {
    return new Promise((resolve, reject) => {
      let govId = this.selectId(singleLocation.governorate, this.governorates);
      let areaId = this.selectId(singleLocation.area, singleLocation.areas);
      var lookUp = this.lookup;
      (function internalRecursive() {
        lookUp.GetallStreets(areaId, singleLocation.block, govId).subscribe((streets) => {
          // this.streets = streets;
          if (streets[0].name == 'Not Found In Paci') {
            console.log('will retry streets');
            internalRecursive();
          } else {
            console.log('got streets');
            singleLocation.streets = streets;
            resolve();
          }
        },
          err => {
            console.log('failed');
          })
      })();

    })
  }

  getByPaci(locationIndex) {
    if (this.newCustomer.locations[locationIndex].paciNumber >= 8) {
      console.log('getting location by PACI');
      this.lookup.GetLocationByPaci(this.newCustomer.locations[locationIndex].paciNumber).subscribe((location) => {
        console.log(location);
        if (location.governorate) {
          this.newCustomer.locations[locationIndex].governorate = location.governorate.name;
        }
        if (location.area) {
          this.newCustomer.locations[locationIndex].areas = [{ name: location.area.name }];
          this.newCustomer.locations[locationIndex].area = location.area.name;
        }
        if (location.block) {
          this.newCustomer.locations[locationIndex].blocks = [{ name: location.block.name }];
          this.newCustomer.locations[locationIndex].block = location.block.name;
        }
        if (location.street) {
          this.newCustomer.locations[locationIndex].streets = [{ name: location.street.name }];
          this.newCustomer.locations[locationIndex].street = location.street.name;
        }
      },
        err => {
          console.log(err);
        })
    } else {

    }

  }

  selectId(name, list) {

    if (name != undefined) {
      console.log(name);
      console.log(list);
      return list.filter((item) => {
        return item.name == name;
      })[0].id;
    }


  }

}
