import {Router} from '@angular/router';
// import {newCallDetails} from './../../api-module/models/newCustomer';
// import {Observable} from 'rxjs';
import {LookupService} from './../../api-module/services/lookup-services/lookup.service';
// import {allCustomerLookup} from './../../api-module/models/lookups-modal';
import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Component, OnInit, ViewChild, Input, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs/Subscription';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-add-new-customer',
  templateUrl: './add-new-customer.component.html',
  styleUrls: ['./add-new-customer.component.css']
})
export class AddNewCustomerComponent implements OnInit, OnDestroy {
  public newCall: any;
  public firstSubmitTry = false;
  public firstStep = false;
  // public CustNewData: any = {};
  lang = localStorage.getItem('lang');
  existedCall: any;

  public selectedTab = 'tab1';
  public prevTab = '';
  files: File;
  base64textString: string;
  // --------- Lookups ---------------
  public callTypes: Subscription;
  public priorities: any[];
  public phoneTypes: any[];
  // public customerTypes: any[];
  public ContractType: Subscription;
  public Locations: Subscription;
  public Governorates: Subscription;
  public existedCustomerSubscription: Subscription;
  public areas: any[];
  public blocks: any[];
  public streets: any[];
  public AddedNewLocation: any = {};
  public TotalPhone = [1];
  public totalPhonesData = [];
  model: any = {};
  public customers: any[] = [];
// ---------------- Location -----------------
  public newLocation: any = {};
  public AddNewLocation: boolean = false;
  public PACI = '';
  public showG: boolean = true;
  public showB: boolean = true;
  public showA: boolean = true;
  public showS: boolean = true;
  lockForm: boolean;
  toggleLocationLoading: boolean;
  @Input() alreadyExisted;

  constructor(private translate: TranslateService,private router: Router, private customerService: CustomerCrudService, private lookupService: LookupService, private alertService: AlertServiceService, private utilities: UtilitiesService, private auth: AuthenticationServicesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    //console.log(this.alreadyExisted);
    !this.alreadyExisted && this.utilities.updateCurrentExistedCustomer({});
    this.existedCustomerSubscription = this.utilities.existedCustomer.subscribe((val) => {
      this.existedCall = val;
      console.log(this.existedCall);
      if (JSON.stringify(this.existedCall) == JSON.stringify({})) {
        this.newCall = {};
        this.lockForm = false;
        this.showG = false;
      } else {
        this.newCall = this.existedCall;
        console.log(this.newCall);
        this.showG = true;
        this.showA = true;
        this.showB = true;
        this.showS = true;
        this.lockForm = true;
      }
      //console.log(this.existedCall);
      //console.log(this.newCall);
    });

    // ----------------- Call all the Lookups ---------------------

    this.lookupService.getCallsTypes().subscribe(types => {
        this.callTypes = types;
        //console.log(this.callTypes);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('we have Server Error !')
      });

    this.lookupService.getPriorities()
      .subscribe((priorities) => {
          this.priorities = priorities;
          //console.log(this.priorities);
        },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();
          //console.log(err);
          this.alertService.error('we have Server Error !')
        });

    this.lookupService.getPhoneTypes().subscribe((phoneTypes) => {
      this.phoneTypes = phoneTypes;
    });

    this.lookupService.getAllCustomers()
            .subscribe((customers) => {
               this.customers = JSON.parse(JSON.stringify(customers));
               console.log(this.customers);
             }, err => { });

    this.lookupService.getContractTypes().subscribe(contractTypes => {
        this.ContractType = contractTypes;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('we have Server Error !')
      });

    this.getAllGovs();

  }

  ngOnDestroy() {
    this.existedCustomerSubscription.unsubscribe();
  }

  editCustomer() {
    this.lockForm = false;
  }

  getFiles(event) {
    this.files = event.target.files[0];
    //console.log(this.files);
    // this.newCust.contract.fileName = event.target.files[0].name;
    var reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsBinaryString(this.files);

  }

  _handleReaderLoaded(readerEvt) {
    var binaryString = readerEvt.target.result;
    this.base64textString = btoa(binaryString);
    // this.newCust.contract.file = btoa(binaryString);
    //console.log(btoa(binaryString));
  }

  getAllGovs() {
    this.lookupService.GetallGovernorates().subscribe(Governorates => {
        if (Governorates[0].name == 'Not Found In Paci') {
          console.log('retry govs');
          this.getAllGovs();
        } else {
          console.log('got govs');
          this.Governorates = Governorates;
        }
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();

        this.alertService.error('we have Server Error !')
      });
  }

  CallArea() {
    this.AddedNewLocation.governorate = this.getSelectedName(this.Governorates, this.newCall.governorate);

    this.lookupService.GetallAreas(this.AddedNewLocation.governorate).subscribe(areas => {
        if (areas[0].name == 'Not Found In Paci') {
          console.log('retry area');
          this.CallArea();
        } else {
          this.areas = areas;
          // if (areas != null) {
          this.showA = false;
          // }
          console.log('got area');
        }

        //console.log(areas);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();

        this.alertService.error('we have Server Error !')
      });
  }

  routeToLogs() {
    // this.router.navigate(['calls-history/logs', this.newCall.id])
    this.router.navigate(['/search/calls-history/logs', this.newCall.id])
  }

  routeToCreateEstimations() {
    // this.router.navigate(['calls-history/logs', this.newCall.id])
    this.router.navigate(['/search/calls-history/estimation', this.newCall.id])
  }

  CallBlock() {
    //console.log('BLOCKS')
    this.AddedNewLocation.area = this.getSelectedName(this.areas, this.newCall.area);
    //console.log(this.AddedNewLocation.area);
    this.lookupService.GetallBlocks(this.AddedNewLocation.area).subscribe(blocks => {
        if (blocks[0].name == 'Not Found In Paci') {
          console.log('retry blocks');
          this.CallBlock();
        } else {
          console.log('got blocks');
          this.blocks = blocks;
          this.showB = false;
        }
        // if (blocks != null) {

        // }
        //console.log(blocks);

      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('we have Server Error !')
      });
  }

  CallStreet() {
    this.AddedNewLocation.block = this.newCall.block;
    //console.log(this.AddedNewLocation.governorate);
    //console.log(this.AddedNewLocation.area);
    //console.log(this.AddedNewLocation.block);
    this.lookupService.GetallStreets(this.AddedNewLocation.area, this.AddedNewLocation.block, this.AddedNewLocation.governorate).subscribe(streets => {
        // this.streets = streets;
        // if (streets != null) {
        //   this.showS = false;
        // }

        if (streets[0].name == 'Not Found In Paci') {
          console.log('retry streets');
          this.CallStreet();
        } else {
          console.log('got streets');
          this.streets = streets;
          this.showS = false;
        }
        //console.log(streets);
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('we have Server Error !')
      });
  }

  CallGetStreat() {
    this.AddedNewLocation.street = this.getSelectedName(this.streets, this.newLocation.street);
  }

  getSelectedName(list, name) {
    //console.log(list)
    //console.log(name)
    if (list != null) {
      for (var i = 0; i < list.length; i++) {
        if (list[i].name == name) {
          return list[i].id;
        }
      }
    }

  }

  checktoSearch(event) {
    //console.log('check to search');
    if (this.newCall.paciNumber && this.newCall.paciNumber.length > 7) {
      this.GetLocationByPaci();
    }
    else {
      this.newLocation = {};
      // this.AddedNewLocation={};
    }
  }

  GetLocationByPaci() {
    //console.log(this.newCall.paciNumber);
    //console.log(this.newCall.paciNumber);
    this.toggleLocationLoading = true;
    // if (this.newCall.paciNumber.length >= 8) {
    this.lookupService.GetLocationByPaci(+this.newCall.paciNumber).subscribe(loc => {
        console.log(loc);
        if (loc != null) {
          //console.log(loc);
          this.newCall.governorate = loc.governorate.name;
          this.showG = false;
          // this.showA = false;
          // this.showB = false;
          // this.showS = false;
          // this.CallArea();
          this.newCall.area = loc.area.name;
          this.areas = [];
          this.areas.push(loc.area);
          // this.CallBlock();
          this.newCall.block = loc.block.name;
          this.blocks = [];
          this.blocks.push(loc.block);
          // this.CallStreet()
          this.newCall.street = loc.street.name;
          this.streets = [];
          this.streets.push(loc.street);
          //console.log("-----------");
          // this.CallGetStreat();

          //console.log(this.newCall);
        }
        this.toggleLocationLoading = false;
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.toggleLocationLoading = false;
        this.alertService.error('we have Server Error !')
      });
    // }
  }

  createNewLocation() {

    // this.AddedNewLocation.fk_Customer_Id= this.id;
    // this.AddedNewLocation.title = this.newLocation.title;

    this.CallGetStreat();

    if (this.AddedNewLocation.paciNumber != "") {
      this.AddedNewLocation.paciNumber = this.PACI;
    }
    //console.log(this.AddedNewLocation);
    //console.log("AddedNewLocation");

    //console.log(this.AddedNewLocation);
    // this.ResetallLocation();
    this.AddNewLocation = false;
    // this.reloadLocation();
  }

  // reloadLocation(){
  //   this.lookupService.GetallLocations().subscribe(locations => {
  //     this.Locations = locations;
  //   },
  //     err => {
  //       this.alertService.error('we have Server Error !')
  //     });
  // }

  ResetallLocation() {
    this.newLocation = {};
    this.newLocation.Government = {};
    this.AddedNewLocation = {};
    this.PACI = '';
    this.showG = false;
    this.showB = true;
    this.showA = true;
    this.showS = true;
    this.AddNewLocation = false;

  }

  onSubmit(form) {

    if (form.valid) {
      //console.log('Valid Form');
      this.AddnewCustomer();
    } else {
      this.firstSubmitTry = true;
      //console.log('In Valid Form');

    }
  }

  AddnewCustomer() {
    console.log(this.newCall);

    this.customerService.addCustomer(this.newCall).subscribe(users => {
        //console.log(" ------------- users -----------");
        //console.log(users);

        this.alertService.success('user added Successfully');
        this.ResetallLocation();
        this.router.navigate(['search/customer/' + users.customer.id]);


      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('Error !!')
      });

  }

  ContinueAdd(values) {
    // let serviceArr = [];
    // console.log(values);
    values.customerServiceName = this.auth.CurrentUser().fullName;
    values.customerServiceUserName = this.auth.CurrentUser().userName;
    values.area = this.newCall.area;
    values.street = this.newCall.street;
    values.block = this.newCall.block;
    values.fK_CallPriority_Id = this.newCall.fK_CallPriority_Id;
    console.log(values);
    this.lookupService.postNewCall(values).subscribe(() => {
        this.alertService.success('Success!');

        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/calls-history']);
        }, 2000);

        // this.routeToCreateEstimations();
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.alertService.error('we have Server Error !');
      });

    //console.log(this.newCall);

  }


  CustomerSelected() {
       let  selectedcustomer = this.customers.find(x => x.id == this.newCall.FK_Customer_Id, 0);
        this.newCall.fK_CallPriority_Id = selectedcustomer.fK_CustomerType_Id;
        console.log(selectedcustomer);
        }



  AddPhoneItem() {
    this.TotalPhone.push(1);
    // this.totalPhonesData = this.newCall.CallerPhones;
    // this.newCust.customer.phones = [];
    this.newCall.CallerPhones = this.totalPhonesData;
  }

  DeletePhoneItem(index) {
    //console.log(this.newCall.CallerPhones);
    //console.log(index);
    this.TotalPhone.splice(index, 1);
    this.newCall.CallerPhones.splice(index, 1);
    this.totalPhonesData = this.newCall.CallerPhones;
    this.newCall.CallerPhones = [];
    this.newCall.CallerPhones = this.totalPhonesData;
    //console.log(this.newCall.CallerPhones)
  }

}
