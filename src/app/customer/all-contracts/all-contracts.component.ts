import { ContractAlmostDoneComponent } from './../contract-almost-done/contract-almost-done.component';
import {Component, OnInit, OnDestroy, ViewChild} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
// import {MenuItem} from
// import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from "primeng/components/common/message";
// import {Router} from "@angular/router";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {GenerateContractModalComponent} from "../generate-contract-modal/generate-contract-modal.component";
import {SearchComponent} from "../../shared-module/search/search.component";
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-all-contracts',
  templateUrl: './all-contracts.component.html',
  styleUrls: ['./all-contracts.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllContractsComponent implements OnInit, OnDestroy {
  allContracts: any[];
  allContractSubscription: Subscription;
  // deleteContractSubscription: Subscription;
  buttonsList: any[];
  // activeRow: any;
  bigMessage: Message[] = [];
 contractMessage: Message[] = [];
  msg: any;
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  allFilteredContracts: any[]=[];
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private lookup: LookupService, private utilities: UtilitiesService, private modalService: NgbModal) {
    
        translate.setDefaultLang(this.lang);
  }

  ngOnInit() {

    if (this.lang == "ar")
      this.msg = "ابحث برقم العقد او تليفون العميل او اسم العميل";
    else if (this.lang == "en")
      this.msg = "Please type contract number, customer phone or customer name to search";
    this.allContracts = [];
    console.log(this.utilities.routingFromAndHaveSearch);
    if (this.utilities.currentSearch != undefined) {
      if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch.searchText && this.utilities.currentSearch.searchType == 'contract') {
        console.log('will search');
        this.searchComponent.searchText = this.utilities.currentSearch.searchText;
        this.searchContract(this.utilities.currentSearch.searchText);
      }
      else {
        console.log('get all');
        this.getAllContracts();
      }
    }
    else {
      console.log('get all');
      this.getAllContracts();
    }
  }

  ngOnDestroy() {
    this.allContractSubscription && this.allContractSubscription.unsubscribe();
  }

  openAddContractModal() {
    console.log('open modal');
    let contractModal = this.modalService.open(GenerateContractModalComponent);
    contractModal.result.then(() => {
      this.getAllContracts();
    })
      .catch(() => {

      })
  }
  openContractsAlmostDone() {
    let contractModal = this.modalService.open(ContractAlmostDoneComponent);
    contractModal.componentInstance.data = this.allFilteredContracts;
    contractModal.result.then(() => {
    
    })
      .catch(() => {

      })
  }


  printContracts(): void {
    var newWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=95%');

    var divToPrint = document.getElementById("contract-table");
    // let newWin = window.open("");
    newWin.document.write(divToPrint.outerHTML);
    newWin.print();
    newWin.close();
  }

  getAllContracts() {
    this.toggleLoading = true;
    this.allContractSubscription = this.lookup.getAllContracts().subscribe((allContracts) => {
        this.toggleLoading = false;
      this.allContracts = allContracts;
       let currentDate=new Date;
      let nextDate=new Date(new Date().setMonth(new Date().getMonth() + 1));
     this.allContracts.forEach(element => {
      let date = new Date(`${ element.endDate}`);
        if((date.getTime()>currentDate.getTime() && date.getTime() < nextDate.getTime()) ){
     this.allFilteredContracts.push(element);
        }
      });
     // this.allFilteredDates=this.allContracts.filter(x=>this.utilities.convertDatetoNormal(x.endDate)>currenNormaltDate && this.utilities.convertDatetoNormal(x.endDate) < nextDate);
        console.log(this.allContracts);
        this.bigMessage = [];
        this.bigMessage.push({
          severity: 'info',
          summary: this.translate.instant('CUSTOMER.DropDownArrow'),
          detail: this.translate.instant('CUSTOMER.remDropDown'),
        });
        this.contractMessage = [];
        this.contractMessage.push({
          severity: 'Error',
          summary: 'Dropdown arrow',
          detail: '<button>Click</button>'
        });
        //console.log(this.allContracts);
        // this.cd.markForCheck();
      },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage = [];
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Contracts due to server error!'
        });
      });
  }

  searchContract(searchText) {
    this.toggleLoading = true;
    console.log(searchText);
    this.utilities.currentSearch.searchType = 'contract';
    this.lookup.searchContract(searchText).subscribe((contracts) => {
        this.allContracts = contracts;
        console.log(this.allContracts);
        this.toggleLoading = false;
        if (!contracts.length) {
          this.cornerMessage.push({
            severity: "error",
            summary: "Failed",
            detail: "Result not found."
          });
        }
      },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        });
        this.toggleLoading = false;
      })
  }

}
