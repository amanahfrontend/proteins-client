import {Component, OnInit, OnDestroy, ChangeDetectionStrategy, ChangeDetectorRef} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Subscription} from "rxjs";
import {Message} from 'primeng/components/common/api';
// import {EditOrderModalComponent} from "../edit-order-modal/edit-order-modal.component"
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-all-orders',
  templateUrl: './all-orders.component.html',
  styleUrls: ['./all-orders.component.css'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})

export class AllOrdersComponent implements OnInit, OnDestroy {
  allOrdersSubscription: Subscription;
  searchSubscription: Subscription;
  allOrders: any[];
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');
  msg: any;
  constructor(private translate: TranslateService, private lookup: LookupService, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    this.getAllOrders();
    if (this.lang == "ar")
      this.msg = "ابحث برقم العقد او تليفون العميل او اسم العميل";
    else if (this.lang == "en")
      this.msg = "Please type contract number, caller name , customer phone or customer name to search ...";
  }

  getAllOrders() {
    this.toggleLoading = true;
    this.allOrdersSubscription = this.lookup.getAllOrders().subscribe((allOrders) => {
        this.toggleLoading = false;
        this.allOrders = allOrders;
        //console.log(this.allOrders);
        this.bigMessage = [];
        this.bigMessage.push({
          severity: "info",
          summary: this.translate.instant('CUSTOMER.DropDownArrow'),
          detail: this.translate.instant('CUSTOMER.remDropDownOrder')
        });
      },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        })
      })
  }

  ngOnDestroy() {
    this.allOrdersSubscription.unsubscribe();
    // this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
    this.searchSubscription && this.searchSubscription.unsubscribe();
  }

  searchByValue(searchText) {
    this.toggleLoading = true;
    this.searchSubscription = this.lookup.searchByOrderNumber(searchText).subscribe((searchResult) => {
        this.allOrders = searchResult;
        this.toggleLoading = false;
        //console.log(searchResult);
        if (!searchResult.length) {
          this.cornerMessage.push({
            severity: "error",
            summary: "Failed",
            detail: "Result not found."
          });
        }
      },
      err => {
        this.toggleLoading = false;
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to network error, please try again later."
        });
      })
  }

}
