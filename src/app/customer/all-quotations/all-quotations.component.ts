import {Component, OnInit, ViewEncapsulation, OnDestroy, ViewChild} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Router} from "@angular/router";
import {MessageService} from 'primeng/components/common/messageservice';
import {Message} from "primeng/components/common/message";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {SearchComponent} from "../../shared-module/search/search.component";
import {Subscription} from "rxjs/Subscription";
import {Angular2Csv} from "angular2-csv";
// import {HubConnection} from '@aspnet/signalr-client';
// import {quotationHubUrl} from '../../../app/api-module/services/globalPath';

@Component({
  selector: 'app-all-quotations',
  templateUrl: 'all-quotations.component.html',
  styleUrls: ['all-quotations.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class AllQuotationsComponent implements OnInit, OnDestroy {
  quotations: any[];
  quotationsSubscription: any;
  buttonsList: any[];
  activeRow: any;
  bigMessage: Message[] = [];
  cornerMessage: Message[] = [];
  roles: string[];
  toggleLoading: boolean;
  // private _hubConnection: HubConnection;
  @ViewChild(SearchComponent)
  searchComponent: SearchComponent;
  notificationSubscription: Subscription;

  constructor(private lookup: LookupService, private router: Router, private messageService: MessageService, private auth: AuthenticationServicesService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.buttonsList = [
      {
        label: 'Rejected', icon: 'fa fa-times', command: () => {
          this.removeQuotation(this.activeRow.id);
        }
      }
    ];
    this.roles = this.auth.CurrentUser().roles;
    this.quotations = [];
    this.notificationSubscription = this.utilities.savedNotificationText.subscribe((value) => {
        if (value) {
          this.searchComponent.searchText = value;
          this.utilities.currentSearch = {searchText: value};
          this.filterQuotations(this.utilities.currentSearch);
        } else if (this.utilities.routingFromAndHaveSearch && this.utilities.currentSearch && this.utilities.currentSearch.searchType == 'quotation') {
          console.log('will search');
          this.searchComponent.searchText = this.utilities.currentSearch.searchText;
          this.searchComponent.fromDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.fromDate);
          this.searchComponent.toDate = this.utilities.convertDateForSearchBinding(this.utilities.currentSearch.toDate);
          this.filterQuotations(this.utilities.currentSearch);
        } else {
          console.log('get all');
          this.getAllQuotations();
        }
      },
      err => {

      });
  }

  setActiveRow(contract) {
    this.activeRow = contract;
    //console.log(this.activeRow);
  }

  ngOnDestroy() {
    this.utilities.setSavedNotificationText('');
    this.quotationsSubscription && this.quotationsSubscription.unsubscribe();
    this.utilities.routingFromAndHaveSearch = false;
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Customer Name': 'Customer Name',
      'Customer Mobile': 'Customer Mobile',
      'Reference no': 'Reference no',
      'Estimation reference no': 'Estimation reference no',
      'Area': 'Area'
    });
    this.quotations.map((item) => {
      exportData.push({
        'Customer Name': item.customerName,
        'Customer Mobile': item.customerMobile,
        'Reference no': item.refNumber,
        'Estimation reference no': item.estimationRefNumber,
        'Area': item.area
      })
    });
    return new Angular2Csv(exportData, 'Contract Report', {
      showLabels: true
    });
  }

  downloadQuotationWord(id) {
    console.log(id);
    this.lookup.downloadQuotationWord(id).subscribe((res) => {
        console.log('success');
        console.log(res);
        let url = res["_body"];
        // this.popupCenter(url,'Download', 150, 50)
        window.open(url, '', '', true);
      },
      err => {
        console.log(err);
        console.log('failed')
      })
  }

  getAllQuotations() {
    this.toggleLoading = true;
    console.log('get all quotations');
    //console.log(this.toggleLoading);
    this.quotationsSubscription = this.lookup.getAllQuotations().subscribe((quotations) => {
        this.toggleLoading = false;
        this.quotations = quotations;
        console.log(this.quotations);
        // this.quotations.map((quotation) => {
        //   quotation.wordUrl = quotationWordUrl + quotation.id;
        // });
        console.log(this.quotations);
        this.bigMessage = [];
        this.bigMessage.push({
          severity: 'info',
          summary: 'Dropdown arrow',
          detail: 'You can remove any Quotation by click the dropdown arrow and choose remove.'
        });
      },
      err => {
        this.toggleLoading = false;
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed',
          detail: 'Failed to load Quotations due to server error!'
        });
      })
  }
      //value.searchText && (value.SearchKey = value.searchText);


  filterQuotations(filterObj) {
    console.log(filterObj);
    this.utilities.currentSearch.searchType = 'quotation';
    this.toggleLoading = true;
    filterObj.searchText && (filterObj.SearchKey = filterObj.searchText);
    // delete filterObj.searchText;
    console.log('filtering');
    let filterObjToPost = Object.assign({}, filterObj);
    delete filterObjToPost.searchText;
    this.lookup.filterQuotations(filterObjToPost).subscribe((quotations) => {
        this.quotations = quotations;
        console.log(this.quotations);
        this.toggleLoading = false;
      },
      (error) => {
        this.cornerMessage.push({
          severity: 'info',
          summary: 'Not found',
          detail: 'This quotation not found.'
        });
        this.toggleLoading = false;
      })
  }

  routeToGenerateContract(refNumber) {
    // this.utilities.setRoutingDataPassed(quotationHeader);
    this.router.navigate(['/search/quotation/', refNumber])
  }

  popupCenter(url, title, w, h) {
    var left = (screen.width/2)-(w/2);
    var top = (screen.height/2)-(h/2);
    return window.open(url, title, 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width='+w+', height='+h+', top='+top+', left='+left);
  }

  routeToViewQuotation(estimationHeader) {
    console.log(estimationHeader);
    this.router.navigate(['search/quotation/viewQuotation/', estimationHeader])
  }

  removeQuotation(id) {
    this.lookup.deleteQuotation(id).subscribe(() => {
        this.quotations.filter((quotation) => {
          return quotation.id != id;
        });
        this.cornerMessage.push({
          severity: 'success',
          summary: 'Success',
          detail: 'Quotation Deleted Successfully!'
        });
        this.quotations = this.quotations.filter((quotation) => {
          return quotation.id != id;
        })
      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.cornerMessage.push({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to delete quotation due to connection error.'
        });
      });
  }

}
