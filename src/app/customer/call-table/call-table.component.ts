import { Component, OnInit, OnDestroy, Input, OnChanges, ViewChild } from '@angular/core';
import { Message } from "primeng/components/common/message";
import { Subscription } from "rxjs";
import { Router, ActivatedRoute } from "@angular/router";
import { AuthenticationServicesService } from "../../api-module/services/authentication/authentication-services.service";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { ConvertCustomerComponent } from '../convert-customer/convert-customer.component';
import { Angular2Csv } from "angular2-csv";
import {TranslateService} from '@ngx-translate/core';
@Component({
  selector: 'app-call-table',
  templateUrl: './call-table.component.html',
  styleUrls: ['./call-table.component.css']
})

export class CallTableComponent implements OnInit, OnDestroy, OnChanges {

  @Input() calls;
  buttonsList: any[];
  cornerMessage: Message[] = [];
  statusSubscription: Subscription;
  roles: string[];
  content: any;
  activeRow: any;
  newAction: any;
  statuses: any[];
  modalRef: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookUp: LookupService, private modalService: NgbModal, private router: Router, private auth: AuthenticationServicesService, private activatedRoute: ActivatedRoute) {
    translate.setDefaultLang(this.lang);
  }
  ngOnChanges() {
    //console.log(this.orders);
    this.calls;
  }

  ngOnInit() {
    this.calls = [];
    this.buttonsList = [
      {
        label: "Add Action", icon: "fa fa-cog", command: () => {
          this.open(this.content, this.activeRow)
        }
      }
    ];
    this.roles = this.auth.CurrentUser().roles;
    console.log(this.activatedRoute);
  }

  ngOnDestroy() {
    this.statusSubscription && this.statusSubscription.unsubscribe();

  }
  // { { row.block } } { { row.street } }, { { row.area } }, { { row.governorate } }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Caller Name': 'Caller Name',
      'Caller Number': 'Caller Number',
      'Call Status': 'Call Status',
      'Governarate': 'Governarate',
      'Area': 'Area',
    });
    this.calls.map((item) => {
      exportData.push({
        'Caller Name': item.callerNumber,
        'Caller Number': item.callerNumber,
        'Call Status': item.callStatus,
        'Governarate': item.governorate,
        'GovernarAreaate': item.area,
      })
    });
    return new Angular2Csv(exportData, 'All Calls almost done', {
      showLabels: true
    });
  }

  setActiveRow(call, content) {
    this.activeRow = call;
    this.content = content;
  }
  convertToCustomer(callData: any) {
    this.router.navigate(['search/newCustomer/', callData.id])
  }

  openModal(data) {
    this.modalRef = this.modalService.open(ConvertCustomerComponent);
    this.modalRef.componentInstance.data = data;
  }


  getStatuses() {
    this.statusSubscription = this.lookUp.getStatues().subscribe((status) => {
      this.statuses = status;
    },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data due to server error"
        })
      })
  }


  routeToGenerateOrder(id) {
    // let contractStringfied = JSON.stringify(contract);
    this.router.navigate(['search/caller/', "generateOrder", id]);
  }

  open(content, call) {
    this.getStatuses();
    this.newAction = {};
    this.modalService.open(content).result
      .then((result) => {
        if (result) {
          result.fk_Call_Id = call.id;
          result.customerServiceName = this.auth.CurrentUser().fullName;
          result.customerServiceUserName = this.auth.CurrentUser().userName;
          //console.log(result);
          this.lookUp.postNewLog(result).subscribe(() => {
            //console.log('posted successfully');
            this.cornerMessage.push({
              severity: "success",
              summary: "Saved successfully",
              detail: "Action Saved to this call Successfully."
            })
          },
            err => {
              this.cornerMessage.push({
                severity: "error",
                summary: "Failed",
                detail: "Failed Save new Action due to server error"
              })
            });
          //console.log(result);
        }
      });
  }

  routeToLogs(callId) {
    this.router.navigate(['search/calls-history/logs/', callId])
  }

  roueToNewEstimation(callId) {
    this.router.navigate(['search/calls-history/estimation/', callId])
  }
}
