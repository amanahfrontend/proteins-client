import { Component, OnInit } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Angular2Csv } from "angular2-csv";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { forEach } from '@angular/router/src/utils/collection';
import { TranslateService } from '@ngx-translate/core';
@Component({
  selector: 'app-customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {
  customers: any[] = [];
  allCustomers: any[] = [];
  toggleLoading: boolean;
  hideDate: boolean=false;
  filteredCustomers: any[] = [];
  s: string;
  lang = localStorage.getItem('lang');
  msg: any;
  constructor(private translate: TranslateService,private lookup: LookupService, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    if (this.lang == "ar")
      this.msg = "ابحث بأسم العميل او رقم الهاتف";
    else if (this.lang == "en")
      this.msg = "Please type customer name or phone number";

    this.toggleLoading = true;
    this._getCustomers();
  }

  _getCustomersAllCustomers() {

    this.customers = this.allCustomers;
  }

  _getCustomers() {
    this.toggleLoading = true;
    this.lookup.getAllCustomers()
      .subscribe((customers) => {
        this.customers = customers;
        this.allCustomers= JSON.parse(JSON.stringify(this.customers));
        console.log(this.customers);
        this.toggleLoading = false;
      },
        err => {
          this.toggleLoading = false;
        })
  }
  filterQustomers(searchData) {
    let searchedString: string = searchData.searchText;
    this.filteredCustomers = [];
    if (isNaN(searchData.searchText)) {

      for (var i = 0; i < this.customers.length; i++) {
        if (this.customers[i].name.toLowerCase().includes(searchedString.toLowerCase())) {
          this.filteredCustomers.push(this.customers[i]);
        }
      }
      this.customers = this.filteredCustomers;
    }
    else {
      for (var i = 0; i < this.customers.length; i++) {
        for (var j = 0; j < this.customers[i].customerPhoneBook.length; j++) {
          if (this.customers[i].customerPhoneBook[j].phone.toLowerCase().includes(searchedString.toLowerCase())) {
            this.filteredCustomers.push(this.customers[i]);
          }

        }

      }
      this.customers = this.filteredCustomers;

    }

  }
  printCustomer() {
    this.utilities.printComponent('customer-table')
  }

  exportCsv() {
    console.log(this.customers);
    let exportData = [];
    exportData.push({
      'Name': 'Contract Number',
      'Type': 'Type',
      'Phone': 'Phone',
      'Address': 'Address'
    });
    this.customers.map((item) => {
      exportData.push({
        'Contract Number': item.name,
        'Type': item.type,
        'Phone': item.customerPhoneBook[0].phone,
        'Address': `${item.locations[0].block}, ${item.locations[0].street}, ${item.locations[0].area}, ${item.locations[0].governorate}`
      })
    });
    return new Angular2Csv(exportData, 'Customers', {
      showLabels: true
    });
  }

}
