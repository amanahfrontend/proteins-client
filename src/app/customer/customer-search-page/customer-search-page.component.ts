import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {Component, OnInit} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-customer-search-page',
  templateUrl: './customer-search-page.component.html',
  styleUrls: ['./customer-search-page.component.css']
})

export class CustomerSearchPageComponent {

  public UserExistance = '';
  public SearchInput = '';
  public allSearchCustomers: any[] = [];
  loading = false;
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private router: Router, private customerService: CustomerCrudService, private alertService: AlertServiceService, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  SearchCustonmer() {
    this.loading = true;
    this.customerService.GoSearchCustomer(this.SearchInput).subscribe(users => {
        this.loading = false;
        //console.log(users);

        if (users.length > 0) {
          this.UserExistance = 'true';
          this.allSearchCustomers = users;
        }
        else {
          this.allSearchCustomers = [];
          this.UserExistance = 'false';
        }

      },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.loading = false;
        this.allSearchCustomers = [];
        this.UserExistance = '';
        this.alertService.error('we have Server Error , please try again !')
      });

  }

  AddNewCustomer() {
    this.router.navigate(['search/new']);
  }

  checktoSearch(event) {
    if (this.SearchInput.length > 4) {
      this.SearchCustonmer();
    }
    else {
      this.allSearchCustomers = [];
    }
  }

  MoreDetail(customer) {
    this.allSearchCustomers = [];
    // if (customer.fK_Customer_Id) {
    //   this.router.navigate(['search/customer/' + customer.fK_Customer_Id]);
    // } else {
      this.router.navigate(['search/customer/' + customer.id]);
    // }
  }

}
