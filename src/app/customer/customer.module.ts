import { ContractAlmostDoneComponent } from './contract-almost-done/contract-almost-done.component';
import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddNewCustomerComponent } from './add-new-customer/add-new-customer.component';
import { CallsHistory } from './call-history/calls-history';
import { CustomerSearchPageComponent } from './customer-search-page/customer-search-page.component';
import { ExistingCustomerComponent } from './existing-customer/existing-customer.component';

import { LogsHistoryComponent } from './logs-history/logs-history.component';
import { EstimationComponent } from './estimation/estimation.component';
import { AllEstimationsComponent } from './all-estimations/all-estimations.component';
import { GenerateQuotationComponent } from './generate-quotation/generate-quotation.component';
import { EstimationTableComponent } from './estimation-table/estimation-table.component';
import { AllQuotationsComponent } from "./all-quotations/all-quotations.component";
import { NewContractModalComponent } from './new-contract-modal/new-contract-modal.component';
import { GenerateContractComponent } from './generate-contract/generate-contract.component';
import { CustomerDetailsComponent } from './customer-details/customer-details.component';
import { AllContractsComponent } from './all-contracts/all-contracts.component';
import { EditEstimationComponent } from "./edit-estimation/edit-estimation.component";
import { GenerateOrderComponent } from './generate-order/generate-order.component';
import { AllOrdersComponent } from './all-orders/all-orders.component';
import { ExistedCustomerContractsComponent } from './existed-customer-contracts/existed-customer-contracts.component';
import { ContractTableComponent } from './contract-table/contract-table.component';
import { OrderTableComponent } from './order-table/order-table.component';
import { CallTableComponent } from './call-table/call-table.component';
import { AddContractFormComponent } from './add-contract-form/add-contract-form.component';
import { GenerateContractModalComponent } from './generate-contract-modal/generate-contract-modal.component';
import { SpinnerModule } from 'primeng/primeng';
import { EstimationReportComponent } from './estimation-report/estimation-report.component';
import { AllEstimationsTableComponent } from './all-estimations-table/all-estimations-table.component';
import { AddEditCustomerComponent } from './add-edit-customer/add-edit-customer.component';
import { CommentModalComponent } from './comment-modal/comment-modal.component';
import { ViewQuotationComponent } from './view-quotation/view-quotation.component';
import { CustomerListComponent } from './customer-list/customer-list.component';
import { AddCustomerFromCallerComponent } from './add-customer-from-caller/add-customer-from-caller.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from "@angular/common/http";
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

const routes: Routes = [
  {
    path: '',
    component: CustomerSearchPageComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'customer/:CurrentCustomer',
    component: ExistingCustomerComponent
  },
  {
    path: 'newCustomer/:id',
    component: AddCustomerFromCallerComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }

  }
  ,
  {
    path: 'new',
    component: AddNewCustomerComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  }
  ,
  {
    path: 'calls-history',
    component: CallsHistory,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'calls-history/logs/:id',
    component: LogsHistoryComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'calls-history/estimation/:id',
    component: EstimationComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'estimation',
    component: AllEstimationsComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'quotation',
    component: AllQuotationsComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['Maintenance', 'CallCenter'] }
  },
  {
    path: 'estimation/:id',
    component: GenerateQuotationComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['Maintenance'] }
  },
  {
    path: 'customersList',
    component: CustomerListComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'customersList/customerDetails',
    component: ExistingCustomerComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'editEstimation/:estimationHeader',
    component: EditEstimationComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['Maintenance', 'CallCenter'] }
  },
  {
    path: 'quotation/viewQuotation/:quotationRefNumber',
    component: ViewQuotationComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  },
  {
    path: 'quotation/:refNumber',
    component: GenerateContractComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'contract',
    component: AllContractsComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'contract/:id',
    component: GenerateOrderComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter'] }
  },
   {
    path: 'caller/:generateOrder/:id',
    component: GenerateOrderComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'order',
    component: AllOrdersComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter'] }
  },
  {
    path: 'estimations-report',
    component: EstimationReportComponent,
    canActivate: [AuthGuardGuard],
    data: { roles: ['CallCenter', 'Maintenance'] }
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    CommonModule,
    FormsModule,
    SharedModuleModule,
    SpinnerModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [AddCustomerFromCallerComponent, ContractAlmostDoneComponent, AddNewCustomerComponent, LogsHistoryComponent, CustomerSearchPageComponent, ExistingCustomerComponent, CallsHistory, EstimationComponent, AllEstimationsComponent, GenerateQuotationComponent, EstimationTableComponent, AllQuotationsComponent, NewContractModalComponent, GenerateContractComponent, CustomerDetailsComponent, AllContractsComponent, EditEstimationComponent, GenerateOrderComponent, AllOrdersComponent, ExistedCustomerContractsComponent, ContractTableComponent, OrderTableComponent, CallTableComponent, AddContractFormComponent, GenerateContractModalComponent, EstimationReportComponent, AllEstimationsTableComponent, AddEditCustomerComponent, CommentModalComponent, ViewQuotationComponent, CustomerListComponent]
  ,
  entryComponents: [
    NewContractModalComponent,
    GenerateContractModalComponent,
    CommentModalComponent,
    ContractAlmostDoneComponent
  ]
})
export class CustomerModule {
}
