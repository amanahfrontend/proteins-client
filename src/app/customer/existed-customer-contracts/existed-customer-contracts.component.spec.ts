import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExistedCustomerContractsComponent } from './existed-customer-contracts.component';

describe('ExistedCustomerContractsComponent', () => {
  let component: ExistedCustomerContractsComponent;
  let fixture: ComponentFixture<ExistedCustomerContractsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExistedCustomerContractsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExistedCustomerContractsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
