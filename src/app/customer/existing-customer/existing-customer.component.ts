// import {
//   customer,
//   contract,
//   complain,
//   CustomerHistory,
//   WorkOrderDetails
// } from './../../api-module/models/customer-model';
import {Subscription} from 'rxjs/Subscription';
import {AlertServiceService} from './../../api-module/services/alertservice/alert-service.service';
import {CustomerCrudService} from './../../api-module/services/customer-crud/customer-crud.service';
import {Router, ActivatedRoute} from '@angular/router';
import {Component, OnInit, OnDestroy} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-existing-customer',
  templateUrl: './existing-customer.component.html',
  styleUrls: ['./existing-customer.component.css']
})

export class ExistingCustomerComponent implements OnInit, OnDestroy {
  // public selectedTab = 'tab1';
  public existCust = false;
  private callId: number;
  private customerId: number;
  private sub: any;
  existedOrders: any[];
  existedCalls: any[];
  callData: any;
  existedCustomerData: any;
  workOrders;
  activeTab: string;
  orderByCustomerSubscription: Subscription;
  callByCustomerSubscription: Subscription;
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private router: Router, private route: ActivatedRoute,
              private customerService: CustomerCrudService, private alertService: AlertServiceService, private utilities: UtilitiesService, private lookupService: LookupService) {
                translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.callData = {};
    // //console.log('existed customer component');
    this.existedOrders = [];
    this.existedCalls = [];
    this.sub = this.route.params.subscribe(params => {
      if (params['customerId']) {
        this.customerId = +params['customerId'];
        console.log('find customer')
        this.getCustomerDetails(this.customerId);
      } else {
        console.log('find call')
        this.callId = +params['CurrentCustomer'];
        this.GetCallDetails();
      }
    });
    this.activeTab = 'info';
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
    this.orderByCustomerSubscription && this.orderByCustomerSubscription.unsubscribe();
    this.callByCustomerSubscription && this.callByCustomerSubscription.unsubscribe();
  }

  setActiveTab(tab) {
    this.activeTab = tab;
    if (tab == 'orders') {
      this.orderByCustomerSubscription = this.lookupService.getOrderByCustomerId(this.callData.fK_Customer_Id).subscribe((orders) => {
          this.existedOrders = orders;
        },
        err => {
          //console.log(err);
        })
    } else if (tab == 'calls') {
      this.callByCustomerSubscription = this.lookupService.getCallByCustomerId(this.callData.callerNumber).subscribe((calls) => {
          this.existedCalls = calls;
        },
        err => {
          //console.log(err);
        })
    }
  }

  getCustomerDetails(customerId) {
    console.log(this.callData.fK_Customer_Id);
    this.lookupService.getCustomerDetails(customerId).subscribe((existedCustomerData) => {
        this.existedCustomerData = existedCustomerData;
        console.log(this.existedCustomerData);
      },
      err => {
        console.log(err);
      })
  }

  GetCallDetails() {
    //console.log('will get customer details')
    this.toggleLoading = true;
    this.customerService.GetCallDetail(this.callId).subscribe(callData => {
        this.callData = callData;
        console.log(this.callData);
        if (this.callData.fK_Customer_Id) {
          this.customerId = this.callData.fK_Customer_Id;
          this.getCustomerDetails(this.customerId);
        } else {
          this.utilities.updateCurrentExistedCustomer(callData);
        }
        this.toggleLoading = false;
      },
      err => {
        this.alertService.error('we have Server Error !')
      });
  }


}
