import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { Subscription } from "rxjs";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { MessageService } from "primeng/components/common/messageservice";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";

@Component({
  selector: 'app-generate-edit-order-form',
  templateUrl: './generate-edit-order-form.component.html',
  styleUrls: ['./generate-edit-order-form.component.css']
})

export class GenerateEditOrderFormComponent implements OnInit, OnDestroy {
  @Output() order = new EventEmitter;
  @Output() close = new EventEmitter;
  @Input() existedOrder: any;
  @Input() hideLocation: boolean;
  @Input() customerId: any;
  @Input() disabled: boolean;
  newOrder: any;
  orderTypeSubscription: Subscription;
  orderStatusSubscription: Subscription;
  orderPrioritySubscription: Subscription;
  orderProblemSubscription: Subscription;
  customerLocationsSubscription: Subscription;
  todayDate: Date;
  orderTypes: any[];
  statuses: any[];
  orderPriority: any[];
  customerLocations: any[];
  problems: any[];
  isNewOrder: boolean;
  orderTypeCashCall:  any[]=[];

  constructor(private lookup: LookupService, private messageService: MessageService, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    //console.log(this.existedOrder);
    if (this.existedOrder) {
      this.isNewOrder = false;
      this.existedOrder.startDate = new Date(this.existedOrder.startDate);
      this.existedOrder.endDate = new Date(this.existedOrder.endDate);
      this.newOrder = this.existedOrder;
      this.customerId = this.existedOrder.fK_Customer_Id;
    } else {
      this.isNewOrder = true;
      this.newOrder = {};
      this.newOrder.fK_OrderStatus_Id = 0;
    }

    //console.log(this.newOrder);

    this.lookup.getOrderType().subscribe((orderTypes) => {
      this.orderTypes = orderTypes;
      if (this.hideLocation == false) {
        let orderCashCall = this.orderTypes.find(ord => ord.name == "Cash Call");
        this.newOrder.fK_OrderType_Id = orderCashCall.id;
        this.orderTypeCashCall.push(orderCashCall);

      }
      else {
             this.orderTypes =  this.orderTypes.filter(ord => ord.name != "Cash Call");
      }
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderPrioritySubscription = this.lookup.getOrderPriority().subscribe((orderPriority) => {
      this.orderPriority = orderPriority;
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderProblemSubscription = this.lookup.getAllProblems().subscribe((problems) => {
      this.problems = problems;
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });
    console.log(this.customerId);
    this.customerLocationsSubscription = this.lookup.getLocationByCustomerId(this.customerId).subscribe((locations) => {
      this.customerLocations = locations;
      console.log(this.customerLocations);
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });

    this.orderStatusSubscription = this.lookup.getOrderStatus().subscribe((status) => {
      this.statuses = status;
      //console.log(this.customerLocations);
    },
      err => {
        err.status == 401 && this.utilities.unauthrizedAction();
        this.messageService.add({
          severity: 'error',
          summary: 'Failed!',
          detail: 'Failed to get data due to network error.'
        })
      });
    this.todayDate = new Date();
    console.log(this.todayDate)
  }

  submitOrder(order) {
    console.log(order);
    this.order.emit(order);
  }

  closeModal() {
    this.close.emit();
  }

  ngOnDestroy() {
    //this.orderTypeSubscription.unsubscribe();
    this.orderPrioritySubscription.unsubscribe();
    this.customerLocationsSubscription.unsubscribe();
  }
}
