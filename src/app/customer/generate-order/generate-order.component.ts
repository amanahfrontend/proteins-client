import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { MessageService } from "primeng/components/common/messageservice";
import { UtilitiesService } from "../../api-module/services/utilities/utilities.service";
import { Message } from "primeng/components/common/message";

@Component({
  selector: 'app-generate-order',
  templateUrl: './generate-order.component.html',
  styleUrls: ['./generate-order.component.css']
})

export class GenerateOrderComponent implements OnInit, OnDestroy {
  contractDetails: any;
  postNewOrderSubscription: Subscription;
  /*this flag to check if the date already converted while post the order*/
  dateConvertedFlag: boolean;
  customerId: any;
  toggleLoading: boolean;
  toggleOrderSectionLoading: boolean;
  todayDate: Date;
  msg: Message[];
  hideInfo: boolean = true;
  callerId: number;

  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService, private messageService: MessageService, private router: Router, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.dateConvertedFlag = false;
    this.utilities.routingFromAndHaveSearch = true;
    this.activatedRoute.params.forEach((params: Params) => {
      if (params['generateOrder'] == "generateOrder") {
        this.hideInfo = false;
        this.callerId = +params['id'];
        this.toggleLoading = false;


      }
      else {
        this.lookup.getContractById(+params['id']).subscribe((contractDetails) => {
          //console.log(contractDetails);
          this.contractDetails = contractDetails;
          this.contractDetails.startDate = this.utilities.convertDatetoNormal(this.contractDetails.startDate);
          this.contractDetails.endDate = this.utilities.convertDatetoNormal(this.contractDetails.endDate);
          this.customerId = this.contractDetails.customer.id;
          this.contractDetails.customer.customerName = this.contractDetails.customer.name;
          this.contractDetails.customer.customerMobile = this.contractDetails.customer.phoneNumber;
          this.toggleLoading = false;
        },
          err => {
            this.toggleLoading = false;
          });
      }

    });
  }

  ngOnDestroy() {
    this.postNewOrderSubscription && this.postNewOrderSubscription.unsubscribe();
  }

  generateOrder(newOrder) {

    if (this.hideInfo == false) {
      this.toggleOrderSectionLoading = true;
      console.log('newOrder');
      console.log(newOrder);
      /*form the json object to be send*/
      newOrder.startDate = this.dateConvertedFlag ? newOrder.startDate : this.utilities.convertDateForSaving(newOrder.startDate);
      newOrder.endDate = this.dateConvertedFlag ? newOrder.endDate : this.utilities.convertDateForSaving(newOrder.endDate);
      newOrder.fK_Customer_Id = null;
      newOrder.fK_Contract_Id = null;
      // newOrder.fK_Location_Id = this.contractDetails.fk_location_id;
      //if (this.contractDetails.contractQuotations[0]) {
      //  newOrder.quotationRefNo = this.contractDetails.contractQuotations[0].quotationRefNumber;
      //}
      // newOrder.price = this.contractDetails.price;

      // //console.log(this.contractDetails.contractQuotations[0]);
      //console.log(newOrder);
      this.dateConvertedFlag = true;
      newOrder.fk_Call_Id = this.callerId;

      /*post the json object*/
      this.postNewOrderSubscription = this.lookup.postNewOrder(newOrder).subscribe(() => {
        this.messageService.add({
          severity: 'success',
          summary: 'Order Generated!',
          detail: 'Order Generated successfully!'
        });
        this.toggleOrderSectionLoading = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/order'])
        }, 2000);
      },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();

          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Failed to generate new Order due to network error.'
          })
          this.toggleOrderSectionLoading = false;
        })


    }

    else {
      this.toggleOrderSectionLoading = true;
      console.log('newOrder');
      console.log(newOrder);
      /*form the json object to be send*/
      newOrder.startDate = this.dateConvertedFlag ? newOrder.startDate : newOrder.startDate.toISOString();
      newOrder.endDate = this.dateConvertedFlag ? newOrder.endDate : newOrder.endDate.toISOString();
      newOrder.fK_Customer_Id = this.contractDetails.customer.id;
      newOrder.fK_Contract_Id = this.contractDetails.id;
      // newOrder.fK_Location_Id = this.contractDetails.fk_location_id;
      if (this.contractDetails.contractQuotations[0]) {
        newOrder.quotationRefNo = this.contractDetails.contractQuotations[0].quotationRefNumber;
      }
      newOrder.price = this.contractDetails.price;
      newOrder.fk_Call_Id = null;

      // //console.log(this.contractDetails.contractQuotations[0]);
      //console.log(newOrder);
      this.dateConvertedFlag = true;
      /*post the json object*/
      this.postNewOrderSubscription = this.lookup.postNewOrder(newOrder).subscribe(() => {
        this.messageService.add({
          severity: 'success',
          summary: 'Order Generated!',
          detail: 'Order Generated successfully!'
        });
        this.toggleOrderSectionLoading = false;
        setTimeout(() => {
          //console.log('will route');
          this.router.navigate(['search/order'])
        }, 2000);
      },
        err => {
          err.status == 401 && this.utilities.unauthrizedAction();

          this.messageService.add({
            severity: 'error',
            summary: 'Failed!',
            detail: 'Failed to generate new Order due to network error.'
          })
          this.toggleOrderSectionLoading = false;
        })
    }

  }

}
