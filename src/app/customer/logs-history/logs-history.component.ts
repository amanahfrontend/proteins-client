import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {ActivatedRoute, Params} from "@angular/router";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Message} from "primeng/components/common/message";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {Location} from '@angular/common';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {TranslateService} from '@ngx-translate/core';
import {CommentModalComponent} from "../comment-modal/comment-modal.component";

@Component({
  selector: 'app-logs-history',
  templateUrl: './logs-history.component.html',
  styleUrls: ['./logs-history.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LogsHistoryComponent implements OnInit {
  logsHistory: any;
  // callId: number;
  cornerMessage: Message[] = [];
  lang = localStorage.getItem('lang');
  constructor(private translate: TranslateService,private route: ActivatedRoute, private lookup: LookupService, private utilities: UtilitiesService, private _location: Location, private NgbModal: NgbModal) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.route.params.forEach((params: Params) => {
      //console.log(+params['id']);
      this.lookup.getCallLogs(+params['id']).subscribe((logs) => {
          //console.log(logs);
          this.logsHistory = logs;
          this.logsHistory.map((log) => {
            log.createdDate = this.utilities.convertDatetoNormal(log.createdDate);
          });
          // this.callId = +params['id'];
        },
        err => {
          this.cornerMessage.push({
            severity: 'error',
            summary: 'Failed',
            detail: 'Failed to load Contracts due to server error!'
          });
          //console.log(err);
        });
    });
  }

  openCommentsDetails(call) {
    let commentModalRef = this.NgbModal.open(CommentModalComponent);
    commentModalRef.componentInstance.data = call;
  }

  backClicked() {
    this.utilities.routingFromAndHaveSearch = true;
    this._location.back();
  }
}
