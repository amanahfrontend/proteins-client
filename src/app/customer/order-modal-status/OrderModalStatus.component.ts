import { Component, OnInit ,Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";

@Component({
  selector: 'app-OrderModalStatus',
  templateUrl: './OrderModalStatus.component.html',
  styleUrls: ['./OrderModalStatus.component.css']
})
export class OrderModalStatusComponent implements OnInit {
    @Input() data: any;

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }
  closeModal() {
    this.activeModal.close();
  }

}
