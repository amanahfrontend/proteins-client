import {Component, OnInit, Input, OnChanges, OnDestroy} from '@angular/core';
import {EditOrderModalComponent} from "../edit-order-modal/edit-order-modal.component"
import {Subscription} from "rxjs";
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Message} from 'primeng/components/common/api';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import { OrderModalStatusComponent } from '../order-modal-status/OrderModalStatus.component';
import { Angular2Csv } from "angular2-csv";


@Component({
  selector: 'app-order-table',
  templateUrl: './order-table.component.html',
  styleUrls: ['./order-table.component.css']
})

export class OrderTableComponent implements OnInit, OnChanges, OnDestroy {
  @Input() orders: any[];
  deleteOrderSubscription: Subscription;
  cornerMessage: Message[] = [];
  modalRef: any;
  buttonsList: any[];
  activeRow: any;

  constructor(private lookup: LookupService, private modalService: NgbModal, private utilities: UtilitiesService) {
  }

  ngOnInit() {
    // this.orders = [];
    this.buttonsList = [
      {
        label: "Remove", icon: "fa fa-times", command: () => {
        this.remove(this.activeRow.id)
      }
      }
    ];
  }

  ngOnChanges() {
    //console.log(this.orders);
    this.orders = this.orders || [];
    this.orders.map((order) => {
      order.startDateView = this.utilities.convertDatetoNormal(order.startDate);

      order.endDateView = this.utilities.convertDatetoNormal(order.endDate);
    });
  }

    exportCsv() {
    let exportData = [];
    exportData.push({
      'Order Number ': 'Order Number',
      'Order Problem ': 'Order Problem ',
      'Order Priority ': 'Order Priority ',
      'Start date': 'Start date',
      'End Date': 'End Date',
      'Price': 'Price',
    });
      this.orders.map((item) => {
        if (item.problem == null)
          item.orderProblem = { "name": "-" }
        if (item.orderPriority == null)
          item.orderPriority = {"name":"-"}
      exportData.push({
        'Order Number': item.code,
        'Order Problem': item.orderProblem.name,
        'Order Priority': item.orderPriority.name,
        'Start date': item.startDate,
        'End Date': item.endDate,
        'Price': item.price,

      })
    });
    return new Angular2Csv(exportData, 'All calls almost done', {
      showLabels: true
    });
  }


  ngOnDestroy() {
    this.deleteOrderSubscription && this.deleteOrderSubscription.unsubscribe();
  }

  setActiveRow(row) {
    this.activeRow = row;
  }

  edit(order) {
    this.openModal(order);
    this.modalRef.result
      .then(() => {
        this.cornerMessage.push({
          severity: "success",
          summary: "Order Saved!",
          detail: "Order edits applied & Saved successfully."
        })
      })
      .catch(() => {
        this.cornerMessage.push({
          severity: "info",
          summary: "Cancelled!",
          detail: "Order Edits cancelled without saving."
        })
      })
  }


  viewOrderStatus(order) {
    this.openModalStatus(order);
    this.modalRef.result
      .then(() => {
        
      })
     
  }


  openModal(data) {
    this.modalRef = this.modalService.open(EditOrderModalComponent);
    this.modalRef.componentInstance.data = data;
  }

  openModalStatus(data) {
    this.modalRef = this.modalService.open(OrderModalStatusComponent);
    this.modalRef.componentInstance.data = data;
  }

  remove(id) {
    //console.log(id);
    this.deleteOrderSubscription = this.lookup.deleteOrder(id).subscribe(() => {
        this.cornerMessage.push({
          severity: "success",
          summary: "Removed successfully",
          detail: "Order removed successfully."
        });
        this.orders = this.orders.filter((order) => {
          return order.id != id;
        })
      },
      err => {
        this.cornerMessage.push({
          severity: "error",
          summary: "Failed",
          detail: "Failed to get data dut to network error, please try again later."
        })
      })
  }

  // private _convertDatetoNormal(date) {
  //   date = new Date(`${date}`);
  //   let year = date.getFullYear();
  //   let month = date.getMonth() + 1;
  //   let day = date.getDate();
  //
  //   if (day < 10) {
  //     day = '0' + day;
  //   }
  //   if (month < 10) {
  //     month = '0' + month;
  //   }
  //   let formatedDate = `${day}/${month}/${year}`;
  //   //console.log(formatedDate);
  //   return formatedDate;
  // }


}
