import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Params, Router} from "@angular/router";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";

@Component({
  selector: 'app-view-quotation',
  templateUrl: './view-quotation.component.html',
  styleUrls: ['./view-quotation.component.css']
})
export class ViewQuotationComponent implements OnInit {
  quotationDetails: any;
  customerDetails: any;
  toggleLoading: boolean;

  constructor(private activatedRoute: ActivatedRoute, private lookup: LookupService) {
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.activatedRoute.params.forEach(
      (param: Params) => {
        let refNumber = param['quotationRefNumber'];
        this.lookup.getQuotationDetailsByRefNo(refNumber)
          .subscribe((quotationDetails) => {
            this.quotationDetails = quotationDetails;
            console.log(quotationDetails);

            this.customerDetails = {
              customerName: quotationDetails.estimation.customerName,
              customerMobile: quotationDetails.estimation.customerMobile,
              area: quotationDetails.estimation.area,
            };
            this.toggleLoading = false;
          })
      }
    )
  }

}
