import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-bulk-assign-modal',
  templateUrl: './bulk-assign-modal.component.html',
  styleUrls: ['./bulk-assign-modal.component.css']
})
export class BulkAssignModalComponent implements OnInit {
  type: any;
  toggleLoading: boolean;
  selectedTarget: any;
  targets: any[];
  @Input() selectedOrders;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private activeModal: NgbActiveModal, private lookup: LookupService, private authService: AuthenticationServicesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.type = undefined;
  }

  assign(value) {
    // console.log(value);
    if (this.type == 'dispatcher') {
      let postObject = {
        fK_Dispatcher_Id: value.selectedTarget,
        fK_Order_Ids: this.selectedOrders
      };
      console.log(postObject);
      this.lookup.assignOrdersToDispatcher(postObject).subscribe(() => {
          this.activeModal.close(this.selectedOrders);
          console.log('success');
        },
        err => {
          console.log('failed');
        })
    } else if (this.type == 'technician') {
      let postObject = {
        fK_Technican_Id: value.selectedTarget,
        fK_Order_Ids: this.selectedOrders
      };
      console.log(postObject);
      this.lookup.assignOrdersToTec(postObject).subscribe(() => {
          this.activeModal.close();
          console.log('success');
        },
        err => {
          console.log('failed');
        })
    }
  }

  selectTargetType() {
    this.toggleLoading = true;
    if (this.type == 'dispatcher') {
      this.lookup.getDispatchers().subscribe((dispatchers) => {
        this.targets = dispatchers;
        this.toggleLoading = false;
      })
    } else if (this.type == 'technician') {
      let dispatcherId = this.authService.CurrentUser().id;
      this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
        console.log(tecs);
        this.targets = tecs;
        this.toggleLoading = false;
      })
    }
  }

  close() {
    this.activeModal.dismiss();
  }
}
