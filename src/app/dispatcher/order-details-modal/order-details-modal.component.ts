import {Component, OnInit, Input} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-order-details-modal',
  templateUrl: './order-details-modal.component.html',
  styleUrls: ['./order-details-modal.component.css']
})
export class OrderDetailsModalComponent implements OnInit {
  @Input() order: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    console.log(this.order);
  }

}
