import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import { Component, OnInit, Output, EventEmitter, Input, OnDestroy, ViewChild } from '@angular/core';
import { LookupService } from "../../api-module/services/lookup-services/lookup.service";
import { Subscription } from "rxjs";
import { AuthenticationServicesService } from "../../api-module/services/authentication/authentication-services.service";
import { DateFromToComponent } from '../../shared-module/date-from-to/date-from-to.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-order-filter',
  templateUrl: './order-filter.component.html',
  styleUrls: ['./order-filter.component.css']
})

export class OrderFilterComponent implements OnInit, OnDestroy {
  filter: any;
  // orderMap: any[];
  allStatus: any[] = [];
  allProblems: any[] = [];
  allDispatchers: any[] = [];
  orderStatusObservable: Subscription;
  problemsObservable: Subscription;
  dispatchersObservable: Subscription;
  tecsByDisSubscription: Subscription;
  tecs: any[];
  startDateFrom: Date;
  startDateTo: Date;
  reportOrders: any[] = [];
  @Input() type: string;
  @Output() todayOrders = new EventEmitter;
  @Output() filteredOrders = new EventEmitter;
  @Output() resetFilter = new EventEmitter;
  @Output() orderProgressMode = new EventEmitter;
  @Output() filterTerms = new EventEmitter;
  lang = localStorage.getItem('lang');
  Created_Date_Start: string;
  Created_Date_End: string;
  Chooseword: string;

  @ViewChild(DateFromToComponent) child: DateFromToComponent;

  constructor(private translate: TranslateService,private lookup: LookupService, private authService: AuthenticationServicesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    if (this.lang == "ar")
    {
      this.Created_Date_Start = "بداية تاريخ الانشاء";
      this.Created_Date_End = "نهاية تاريخ الانشاء";  
      this.Chooseword = "اختار";  
    }
  else if (this.lang == "en")
  {
    this.Created_Date_Start = "Created Date Start";
    this.Created_Date_End = "Created Date End";  
    this.Chooseword = "Choose";  
  }
    this.filter = {};
    // this.orderMap = [];
    this.getAllStatus();
    this.getAllProblems();
    this.getAllDispatchers();
    this.getTecsByDis();
  }

  ngOnDestroy() {
    this.orderStatusObservable && this.orderStatusObservable.unsubscribe();
    this.problemsObservable && this.problemsObservable.unsubscribe();
    this.dispatchersObservable && this.dispatchersObservable.unsubscribe();
    this.tecsByDisSubscription && this.tecsByDisSubscription.unsubscribe();
  }

  exportCsv() {
    let exportData = [];
    exportData.push({
      'Code': 'Code',
      'Created Date': 'Created Date',
      'Status': 'Status',
      'Type': 'Type',
      'Priority': 'Priority',
      'Problem': 'Problem',
      'Customer Name': 'Customer Name',
      'Customer Phone': 'Customer Phone',
      'Governorate': 'Governorate',
      'Area': 'Area',
      'Block': 'Block',
      'Street': 'Street',
      'Dispatcher Name': 'Dispatcher Name',
      'Technician Name': 'Technician Name',
      'Order Progress': 'Order Progress',
      'Progress Date': 'Progress Date',
      'Progress Creator': 'Progress Creator',
      'Progress by Tecnician': 'Progress by Tecnician'
    });
    this.reportOrders.map((order) => {
      exportData.push({
        'Code': order.order.code,
        'Created Date': order.createdDateView,
        'Status': order.order.orderStatus.name,
        'Type': order.order.orderType.name,
        'Priority': order.order.orderPriority.name,
        'Problem': order.order.orderProblem.name,
        'Customer Name': order.order.customer.name,
        'Customer Phone': order.phoneView,
        'Governorate': order.order.location.governorate,
        'Area': order.order.location.area,
        'Block': order.order.location.block,
        'Street': order.order.location.street,
        'Dispatcher Name': order.order.dispatcherName,
        'Technician Name': order.order.technicanName,
        'Order Progress': order.orderProgress && order.orderProgress.progressStatus.name,
        'Progress Date': order.orderProgress && order.orderProgress.createdDateView,
        'Progress Creator': order.orderProgress && order.orderProgress.createdBy.userName,
        'Progress by Tecnician': order.orderProgress && order.orderProgress.technicanName
      })
    });
    let reportName = 'Report' + Date.now();
    return new Angular2Csv(exportData, reportName, {
      showLabels: true
      // showTitle: true
    });
  }

  setStartDateFrom(date) {
    this.filter.startDateFrom = date;
  }

  setStartDateTo(date) {
    this.filter.startDateTo = date;
  }

  applyFilter() {
    // let dispatcherId = this.authService.CurrentUser().id;
    console.log(this.filter);
    let filterStatus = [];
    let filterProblems = [];
    let filterdis = [];
    let filterTecs = [];
    let startDateFrom;
    let startDateTo;
    this.filter.status && this.filter.status.map((status) => {
      filterStatus.push(status.id);
    });
    this.filter.problems && this.filter.problems.map((problem) => {
      filterProblems.push(problem.id);
    });
    this.filter.dispatchers && this.filter.dispatchers.map((dis) => {
      filterdis.push(dis.id);
    });
    this.filter.tecs && this.filter.tecs.map((tec) => {
      filterTecs.push(tec.id);
    });
    startDateFrom = this.filter.startDateFrom && this.filter.startDateFrom.toISOString();
    startDateTo = this.filter.startDateTo && this.filter.startDateTo.toISOString();
    let filterObjectToPost = {
      FK_Dispatcher_Ids: filterdis,
      FK_OrderStatus_Ids: filterStatus,
      FK_OrderProblem_Ids: filterProblems,
      FK_Technican_Ids: filterTecs,
      CreatedDateFrom: startDateFrom,
      CreatedDateTo: startDateTo,
      HasOrderProgress: this.filter.hasOrderProgress
    };
    console.log(filterObjectToPost);
    if (this.type == 'order board') {
      this.filterTerms.emit(this.filter);
    } else {
      this.postFilterProperties(filterObjectToPost);
    }
  }

  postFilterProperties(filterObjectToPost) {
    if (this.type == 'report') {
      this.lookup.filterOrders(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        this.reportOrders = res;
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);

        // this.todayOrdersStatus = false;
      },
        err => {

        });
    } else {
      this.lookup.filterOrdersMap(filterObjectToPost).subscribe((res) => {
        console.log(res);
        this.filteredOrders.emit(res);
        console.log(this.filter.hasOrderProgress);
        this.orderProgressMode.emit(this.filter.hasOrderProgress);
        // this.todayOrdersStatus = false;
      },
        err => {

        });
    }
  }

  emitTodayOrders() {
    this.filter = {};
    this.todayOrders.emit();
  }

  checkFilter() {
    let checkingObject = Object.assign({}, this.filter);
    delete checkingObject.hasOrderProgress;
    for (let property in checkingObject) {
      // console.log(property);
      if (checkingObject.hasOwnProperty(property) && (checkingObject['' + property + ''].length || checkingObject.startDateFrom || checkingObject.startDateTo)) {
        // console.log('will return false');
        return false;
      } else {
        // console.log('will return true');
        return true;
      }
    }
    if (JSON.stringify(checkingObject) == JSON.stringify({})) {
      // console.log('will return true because filter is clear');
      return true;
    }
  }

  applyResetFilter() {
    this.filter = {};
    this.reportOrders = [];
    this.child.resetStartDate();
    this.child.resetEndDate();

    this.resetFilter.emit();
  }

  getAllProblems() {
    this.problemsObservable = this.lookup.getAllProblems().subscribe((problems) => {
      this.allProblems = problems;
      console.log(this.allProblems);
    },
      err => {
        console.log('')
      })
  }

  getAllStatus() {
    this.orderStatusObservable = this.lookup.getOrderStatus().subscribe((status) => {
      this.allStatus = status;
    },
      err => {

      })
  }

  getAllDispatchers() {
    this.dispatchersObservable = this.lookup.getDispatchers().subscribe((dispatchers) => {
      this.allDispatchers = dispatchers;
    },
      err => {
        console.log('failed')
      })
  }

  getTecsByDis() {
    let dispatcherId = this.authService.CurrentUser().id;
    this.tecsByDisSubscription = this.lookup.getTecsByDis(dispatcherId).subscribe((tecs) => {
      this.tecs = tecs;
    },
      err => {

      })
  }


}
