import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {AlertServiceService} from "../../api-module/services/alertservice/alert-service.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-order-progress-modal',
  templateUrl: './order-progress-modal.component.html',
  styleUrls: ['./order-progress-modal.component.css']
})

export class OrderProgressModalComponent implements OnInit {
  @Input() order: any;
  progress: any[];
  newProgress: any;
  progressStatuses: any[];
  toggleAddFormFlag: boolean;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,public activeModal: NgbActiveModal, private lookupService: LookupService, private utilitiesService: UtilitiesService, private alertService: AlertServiceService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.getOrderProgress();
    this.getProgressStatuses();
    this.newProgress = {};
    this.toggleAddFormFlag = false;
  }

  submitProgress(progress) {
    console.log(this.order);
    let progressToSend = {
      fK_ProgressStatus_Id: Number(progress.fK_ProgressStatus_Id),
      note: progress.note,
      fK_Order_Id: this.order.id,
      fK_CreatedBy_Id: this.order.fK_Dispatcher_Id,
      fK_Technican_Id: this.order.fK_Technican_Id
    };
    console.log(progressToSend);
    this.lookupService.postProgress(progressToSend).subscribe(() => {
        this.close();
      },
      err => {
        if (!progressToSend.fK_Technican_Id) {
          this.alertService.error(this.translate.instant("MSG.This_order_not_assigned_to_Technician"))
        }
      })
  }

  getOrderProgress() {
    console.log(this.order.id);
    this.lookupService.getOrderProgress(this.order.id).subscribe((progress) => {
        this.progress = progress;
        this.progress && this.progress.map((item) => {
          item.createdDate = this.utilitiesService.convertDatetoNormal(item.createdDate);
        });
        console.log(this.progress);
      },
      err => {

      })
  }

  getProgressStatuses() {
    this.lookupService.getProgressStatuses().subscribe((statuses) => {
        this.progressStatuses = statuses;
      },
      err => {

      })
  }

  toggleAddForm() {
    this.toggleAddFormFlag = !this.toggleAddFormFlag;
  }

  close() {
    this.activeModal.dismiss();
  }
}
