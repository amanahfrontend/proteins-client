import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {LookupService} from '../../../api-module/services/lookup-services/lookup.service';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-preventive-details-modal',
  templateUrl: './preventive-details-modal.component.html',
  styleUrls: ['./preventive-details-modal.component.css']
})

export class PreventiveDetailsModalComponent implements OnInit {
  @Input() data;
  toggleLoading: boolean;
  orderProgress: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,public activeModal: NgbActiveModal, private lookup: LookupService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    console.log('init data');
    this.data.data.customer.phone = this.data.data.customer.customerPhoneBook[0].phone;
    console.log(this.data);
    this.data.fK_Order_Id = 431;
    if (this.data.fK_Order_Id){
      this.lookup.getOrderProgress(this.data.fK_Order_Id).subscribe((progress) => {
        if (progress && progress.length > 0) {
          this.orderProgress = progress;
          console.log('progress');
          console.log(this.orderProgress);
        }
      });
    }
  }

  close() {
    this.activeModal.dismiss();
  }
}
