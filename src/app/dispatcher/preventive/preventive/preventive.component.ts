import {Component, OnInit} from '@angular/core';
import {LookupService} from '../../../api-module/services/lookup-services/lookup.service';
import {AuthenticationServicesService} from '../../../api-module/services/authentication/authentication-services.service';
import {UtilitiesService} from '../../../api-module/services/utilities/utilities.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PreventiveDetailsModalComponent} from '../preventive-details-modal/preventive-details-modal.component';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-preventive',
  templateUrl: './preventive.component.html',
  styleUrls: ['./preventive.component.css']
})
export class PreventiveComponent implements OnInit {
  orders: any;
  savedOrders: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookup: LookupService, private auth: AuthenticationServicesService, private utilities: UtilitiesService, private modalService: NgbModal) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.getAllOrders();
  }

  getAllOrders() {
    this.lookup.getAllPreventive().subscribe((orders) => {
      if (orders) {
        orders.map(item => {
          if (item.customer.customerPhoneBook.length > 0) {
            item.customer.phone = item.customer.customerPhoneBook[0].phone;
          } else {
            item.customer.phone = '--';
          }
          item.contract.endDate = this.utilities.convertDatetoNormal(item.contract.endDate);
          item.contract.startDate = this.utilities.convertDatetoNormal(item.contract.startDate);
          item.orderDate = this.utilities.convertDatetoNormal(item.orderDate);
        });
        this.orders = orders;
        console.log(orders);
      }
    });
  }

  filterPreventive(searchTerms) {
    /* this.toggleLoading = true;*/
    searchTerms.contractNumbers = [searchTerms.searchText];
    delete searchTerms.searchText;
    console.log(searchTerms);
    this.lookup.filterPreventive(searchTerms).subscribe((orders) => {
        this.orders = orders;
        console.log(this.orders);
        this.orders.map((order) => {
          order.contract.startDate = this.utilities.convertDatetoNormal(order.contract.startDate);
          order.contract.endDate = this.utilities.convertDatetoNormal(order.contract.endDate);
          order.orderDate = this.utilities.convertDatetoNormal(order.orderDate);
        })
      },
      err => {
        console.log(err);
      });
    // code.value.toDate = this.utilities.convertDatetoNormal(code.value.toDate);
    // code.value.fromDate = this.utilities.convertDatetoNormal(code.value.fromDate);
    // this.savedOrders = this.orders;
    // this.orders = this.orders.filter(sn =>
    //   sn.contract.contractNumber.toLowerCase() === code.value.searchText.toLowerCase() &&
    //   sn.orderDate > code.value.fromDate &&
    //   sn.orderDate < code.value.toDate);
  }

  preventiveDetails(data) {
    const preventiveModalRef = this.modalService.open(PreventiveDetailsModalComponent);
    preventiveModalRef.componentInstance.data = {
      data: data
    };
  }
}
