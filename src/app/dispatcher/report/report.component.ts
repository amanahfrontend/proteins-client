import {Component, OnInit, ViewChild} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {OrderFilterComponent} from "../order-filter/order-filter.component";
import {TranslateService} from '@ngx-translate/core';
// import {Angular2Csv} from 'angular2-csv/Angular2-csv';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css']
})
export class ReportComponent implements OnInit {
  orders: any[] = [];
  orderProgressMode: boolean = false;
  @ViewChild(OrderFilterComponent) orderFilterComponent: OrderFilterComponent;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookup: LookupService, private auth: AuthenticationServicesService, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
  }

  applyFilter(filteredOrders) {
    this.orders = filteredOrders;
    this.orders.map((order) => {
      order.createdDateView = this.utilities.convertDatetoNormal(order.order.createdDate);
      order.phoneView = order.order.customer.customerPhoneBook[0].phone
    });
    console.log(this.orderProgressMode);
    if (this.orderProgressMode) {
      this.orders.map((order) => {
        order.orderProgress && (order.orderProgress.createdDateView = this.utilities.convertDatetoNormal(order.orderProgress.createdDate));
      })
    }
  }

  // exportCsv() {
  //   let exportData = [];
  //   exportData.push({
  //     'Code': 'Code',
  //     'Created Date': 'Created Date',
  //     'Status': 'Status',
  //     'Type': 'Type',
  //     'Priority': 'Priority',
  //     'Problem': 'Problem',
  //     'Customer Name': 'Customer Name',
  //     'Customer Phone': 'Customer Phone',
  //     'Governorate': 'Governorate',
  //     'Area': 'Area',
  //     'Block': 'Block',
  //     'Street': 'Street',
  //     'Dispatcher Name': 'Dispatcher Name',
  //     'Technician Name': 'Technician Name',
  //     'Order Progress': 'Order Progress',
  //     'Progress Date': 'Progress Date',
  //     'Progress Creator': 'Progress Creator',
  //     'Progress by Tecnician': 'Progress by Tecnician'
  //   });
  //   this.orders.map((order) => {
  //     exportData.push({
  //       'Code': order.order.code,
  //       'Created Date': order.createdDateView,
  //       'Status': order.order.orderStatus.name,
  //       'Type': order.order.orderType.name,
  //       'Priority': order.order.orderPriority.name,
  //       'Problem': order.order.orderProblem.name,
  //       'Customer Name': order.order.customer.name,
  //       'Customer Phone': order.phoneView,
  //       'Governorate': order.order.location.governorate,
  //       'Area': order.order.location.area,
  //       'Block': order.order.location.block,
  //       'Street': order.order.location.street,
  //       'Dispatcher Name': order.order.dispatcherName,
  //       'Technician Name': order.order.technicanName,
  //       'Order Progress': order.orderProgress.progressStatus.name,
  //       'Progress Date': order.orderProgress.createdDateView,
  //       'Progress Creator': order.orderProgress.createdBy.userName,
  //       'Progress by Tecnician': order.orderProgress.technicanName
  //     })
  //   });
  //   let reportName = 'Report' + Date.now();
  //   return new Angular2Csv(exportData, 'Report', {
  //     showLabels: true
  //     // showTitle: true
  //   });
  // }

  applyOrderProgressMode(mode) {
    console.log(mode);
    this.orderProgressMode = mode;
  }

  getAllOrdersByDis() {
    // let dispatcherId = this.auth.CurrentUser().id;
    // this.lookup.getDisAllOrders(dispatcherId).subscribe((dis) => {
    //     this.orders = dis.orders;
    //     console.log(this.orders);
    //   },
    //   err => {
    //
    //   });
    this.orders = [];
  }

}
