import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {Message} from "primeng/components/common/message";
import {TranslateService} from '@ngx-translate/core';
// import {objectLiteralExpression} from "codelyzer/util/astQuery";

@Component({
  selector: 'app-set-time-modal',
  templateUrl: './set-time-modal.component.html',
  styleUrls: ['./set-time-modal.component.css']
})
export class SetTimeModalComponent implements OnInit {
  @Input() order: any;
  day: any;
  cornerMessage: Message[];
  dateToShowOnCal: any;
  defaultDate: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private activeModal: NgbActiveModal, private lookup: LookupService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.dateToShowOnCal = {};
    console.log(this.order.preferedVisitTime);
    if (this.order.preferedVisitTime.toString() !== '0001-01-01T00:00:00') {
      console.log('in if');
      console.log(this.order.preferedVisitTime);
      this.order.preferedVisitTime = new Date(this.order.preferedVisitTime);
      let oldDate = new Date(this.order.preferedVisitTime);
      this.dateToShowOnCal = {
        day: oldDate.getDate(),
        month: oldDate.getMonth() + 1,
        year: oldDate.getFullYear()
      }
    } else {
      console.log('in else');
      this.defaultDate = new Date();
      this.order.preferedVisitTime = this.defaultDate;
    }
  }

  setTime(time) {
    this.cornerMessage = [];
    let choosedTime: any = new Date(time.day);
    console.log(time);
    choosedTime = choosedTime.toISOString();
    console.log(choosedTime);
    let toPost = {
      preferedVisitTime: choosedTime,
      fK_Order_Id: this.order.id
    };
    console.log(toPost);
    this.lookup.postVisitTime(toPost).subscribe(() => {
        this.cornerMessage.push({
          severity: this.translate.instant("MSG.suces"),
          summary: this.translate.instant("MSG.sucesful"),
          detail: this.translate.instant("MSG.Date_set_Successfully")
        });
        this.activeModal.close();
      },
      err => {
        this.cornerMessage.push({
          severity: this.translate.instant("MSG.Error_Msg"),
          summary: this.translate.instant("MSG.fail"),
          detail: this.translate.instant("MSG.Failed_to_save_visit")
        })
      });
  }

  close() {
    this.activeModal.dismiss();
  }
}
