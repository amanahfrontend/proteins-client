import {Component, OnInit, Input} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-transfer-dispatcher-modal',
  templateUrl: './transfer-dispatcher-modal.component.html',
  styleUrls: ['./transfer-dispatcher-modal.component.css']
})

export class TransferDispatcherModalComponent implements OnInit {
  @Input() order;
  dispatchers: any[];
  choosedDispatcher: any;
  toggleLoading: boolean;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,public activeModal: NgbActiveModal, private lookupService: LookupService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.toggleLoading = true;
    this.getDispatchers();
  }

  chooseDispatcher(dispatcher) {
    console.log(dispatcher);
    let transferToDispatcher = {
      fK_Dispatcher_Id: dispatcher.id,
      fK_Order_Id: this.order.id
    };
    this.lookupService.postTransferOrder(transferToDispatcher).subscribe(() => {
        console.log('success');
        this.activeModal.close();
      },
      err => {
        console.log('failed');
      })
  }

  getDispatchers() {
    this.lookupService.getDispatchers().subscribe((dispatchers) => {
        console.log(dispatchers);
        this.dispatchers = dispatchers;
        this.toggleLoading = false;
      },
      err => {

      })
  }

  close() {
    this.activeModal.dismiss();
  }
}
