import {Component, OnInit} from '@angular/core';
import {LookupService} from "../../api-module/services/lookup-services/lookup.service";
import {AuthenticationServicesService} from "../../api-module/services/authentication/authentication-services.service";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {OrderDetailsModalComponent} from "../order-details-modal/order-details-modal.component";
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-visit-time-calender',
  templateUrl: './visit-time-calender.component.html',
  styleUrls: ['./visit-time-calender.component.css']
})
export class VisitTimeCalenderComponent implements OnInit {
  orders: any[];
  header: any;
  calenderOrders: any[];
  orderDetailsRef: any;
  statusColors: any;
    allOrders: any;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private lookup: LookupService, private auth: AuthenticationServicesService, private modalService: NgbModal, private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.getDispatcherOrders();
    this.statusColors = this.utilities.statusColors;
    this.header = {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay'
    };
  }

  showOrderDetails(e) {
      this.orders = this.allOrders;
    console.log(e.calEvent);
    this.orderDetailsRef = this.modalService.open(OrderDetailsModalComponent, {
      size: "sm"
    });
    console.log(e.calEvent.id);
    this.orderDetailsRef.componentInstance.order = this.orders.filter((order) => {
      console.log(order.id);
      console.log(e.calEvent.id);
      return order.id == e.calEvent.id
    })[0];
    this.orderDetailsRef.result
      .then(() => {

      })
      .catch(() => {

      })
  }

  getDispatcherOrders() {
    let dispatcherId = this.auth.CurrentUser().id;
    this.lookup.getDisAllOrders(dispatcherId).subscribe((orders) => {
      console.log(orders);
      this.allOrders = orders.orders;
        this.orders = this.utilities.setOrdersColorByState(orders.orders);
        this.calenderOrders = [];
        orders.orders.map((order) => {
          this.calenderOrders.push({
            id: order.id,
            title: order.code,
            start: order.preferedVisitTime,
            end: order.preferedVisitTime,
            color: order.color
            // id: order.id
          })
        });
        console.log(this.calenderOrders)
      },
      err => {

      })
  }


}
