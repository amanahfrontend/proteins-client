import { Component, OnInit } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
    lang = localStorage.getItem('lang');

    currentUser: any;

    constructor(private translate: TranslateService) {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        translate.setDefaultLang(this.lang);
    }

    ngOnInit() {
        // this.loadAllUsers();
    }

}