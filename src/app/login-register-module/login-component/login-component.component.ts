import { AuthenticationServicesService } from "./../../api-module/services/authentication/authentication-services.service";
import { AlertServiceService } from "./../../api-module/services/alertservice/alert-service.service";
import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import {TranslateService} from '@ngx-translate/core';



@Component({
  selector: "app-login-component",
  templateUrl: "./login-component.component.html",
  styleUrls: ["./login-component.component.css"]
})
export class LoginComponentComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
    lang = localStorage.getItem('lang');

  // private _hubConnection: HubConnection;

  constructor(private route: ActivatedRoute,private translate: TranslateService,
    private router: Router,
    private authenticationService: AuthenticationServicesService,
    private alertService: AlertServiceService) {
                    translate.setDefaultLang(this.lang);

  }

  ngOnInit() {
    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams["returnUrl"] || "/";
  }

  login() {
    this.loading = true;
    this.authenticationService.login(this.model.username, this.model.password)
      .subscribe(
        loginResponse => {
          // console.log(this.returnUrl);
          this.setLocalStorage("userId", loginResponse.id);

          this.router.navigate(["./" + this.returnUrl]);
          this.authenticationService.activateSignalR();
          // this.hubInit();
        },
        error => {
          //console.log(error);
          this.alertService.error("User name or password is incorrect");
          this.loading = false;
        });
  }

  setLocalStorage(key, value) {
    localStorage.setItem(key, value);
  }
}
