import { FormsModule } from '@angular/forms';
import { AuthGuardGuard } from './../api-module/guards/auth-guard.guard';
import { RouterModule, Routes } from '@angular/router';
import { SharedModuleModule } from './../shared-module/shared-module.module';
import { LoginComponentComponent } from './../login-register-module/login-component/login-component.component';
import { HomePageComponent } from './../login-register-module/home-page/home-page.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, '../assets/i18n/', '.json');
}
const routes: Routes = [
  { path: '', component: HomePageComponent, canActivate: [AuthGuardGuard],data: { roles: ['SupervisorDispatcher', 'CallCenter', 'Maintenance', 'Admin', 'Dispatcher'] }},
  { path: 'login', component: LoginComponentComponent },
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModuleModule,
    RouterModule.forChild(routes),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })

  ],
  declarations: [
    LoginComponentComponent,
    HomePageComponent
  ]
})
export class LoginRegisterModuleModule { }
