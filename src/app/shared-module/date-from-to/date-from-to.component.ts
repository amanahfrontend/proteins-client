import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-date-from-to',
  templateUrl: './date-from-to.component.html',
  styleUrls: ['./date-from-to.component.css']
})

export class DateFromToComponent implements OnInit {
  @Input() submitted: boolean;
  @Input() required: boolean;
  @Input() layout: string;
  @Input() startDateLable: string;
  @Input() endDateLable: string;
  condition: boolean = true;
  date: any = {};
  layoutClass: string;
  @Output() startDate = new EventEmitter();
  @Output() endDate = new EventEmitter();
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService) {
    translate.setDefaultLang(this.lang);
  }

  ngOnInit() {
    this.layoutClass = this.layout;
    console.log(this.required);
    console.log(this.layoutClass);
  }

  emitStartDate() {
    this.startDate.emit(this.date.startDate);

  }
  resetStartDate() {
    this.date.startDate = null;
    this.condition = false; 
    this.condition = true;

  }
  resetEndDate() {
    this.date.endDate = null;
    this.condition = false;
    this.condition = true;
  }
  emitEndDate() {
    this.endDate.emit(this.date.endDate);
  }
}
