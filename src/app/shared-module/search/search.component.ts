import {Component, OnInit, ViewEncapsulation, Output, Input, EventEmitter} from '@angular/core';
import {UtilitiesService} from "../../api-module/services/utilities/utilities.service";
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class SearchComponent implements OnInit {
  @Input() placholder: string;
  @Input() hideFromTo: boolean;
  @Input() hideDate: boolean=true;
  @Output() searchValue = new EventEmitter;
  @Output() searchCanceled = new EventEmitter;
  searchText: string;
  fromDate: any;
  toDate: any;
  errorFlag: boolean;
  submitted: boolean = false;
  lang = localStorage.getItem('lang');

  constructor(private translate: TranslateService,private utilities: UtilitiesService) {
    translate.setDefaultLang(this.lang);
  }
  
  ngOnInit() {
    console.log(this.fromDate);
    console.log(this.toDate);
    console.log('init search component');
    if (this.hideFromTo === undefined) {
      this.hideFromTo = true;
    } else {
      this.hideFromTo = false;
    }
  }
  
  clearSearch() {
    this.errorFlag = false;
    this.searchText = '';
    this.fromDate = '';
    this.toDate = '';
    this.utilities.currentSearch = this.searchText;
    this.searchCanceled.emit();
  }
  
  toggleErrorFlag() {
    this.errorFlag = true;
  }
  
  outPutSearchValue(formValues) {
    this.submitted = true;
    console.log(formValues);
    console.log(this.toDate);
    if (!this.hideFromTo) {
      console.log(formValues.value);
      let fromDateToPost = formValues.value.fromDate && new Date(formValues.value.fromDate);
      let toDateToPost = formValues.value.toDate && new Date(formValues.value.toDate);
      // console.log(fromDateToPost.toISOString());
      let searchObject = {};
      if (formValues.value.searchText || fromDateToPost || toDateToPost) {
        formValues.value.searchText && (searchObject['searchText'] = formValues.value.searchText);
        fromDateToPost && (searchObject['fromDate'] = fromDateToPost.toISOString());
        toDateToPost && (searchObject['toDate'] = toDateToPost.toISOString());
      }
      console.log(searchObject);
      this.utilities.currentSearch = searchObject;
      this.searchValue.emit(searchObject);
    } else {
      this.utilities.currentSearch = {searchText: formValues.value.searchText};
      this.searchValue.emit(formValues.value.searchText);
    }
  }
}
