import {CustomerCrudService} from './../api-module/services/customer-crud/customer-crud.service';
import {FormsModule} from '@angular/forms';
import {AlertServiceService} from './../api-module/services/alertservice/alert-service.service';
import {AlertComponentComponent} from './shared/alert-component/alert-component.component';
import {FooterComponentComponent} from './footer-component/footer-component.component';
import {MainHeaderComponentComponent} from './main-header-component/main-header-component.component';
import {ApiModuleModule} from './../api-module/api-module.module';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ModuleWithProviders, Type} from '@angular/core';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
// import {DragulaModule, DragulaService} from "ng2-dragula/ng2-dragula"
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {PromptComponent} from './shared/prompt/prompt.component';
import {SearchComponent} from './search/search.component';
import {EditOrderModalComponent} from '../customer/edit-order-modal/edit-order-modal.component'

import {GenerateEditOrderFormComponent} from "../customer/generate-edit-order-form/generate-edit-order-form.component"

import {SplitButtonModule} from 'primeng/components/splitbutton/splitbutton';
import {CalendarModule} from 'primeng/components/calendar/calendar';
import {GrowlModule} from 'primeng/components/growl/growl';
import {AccordionModule} from 'primeng/components/accordion/accordion';
import {MessagesModule} from 'primeng/components/messages/messages';
import {MessageModule} from 'primeng/components/message/message';
import {AddEditUserModalComponent} from "../admin/admin-users/add-edit-user-modal/add-edit-user-modal.component";
import {FileUploadModule} from 'primeng/components/fileupload/fileupload';
import {ProgressSpinnerModule} from 'primeng/components/progressspinner/progressspinner';
import {TooltipModule} from 'primeng/components/tooltip/tooltip';
import {InputSwitchModule} from 'primeng/components/inputswitch/inputswitch';
import {DropdownModule} from 'primeng/components/dropdown/dropdown';
import {MultiSelectModule} from 'primeng/components/multiselect/multiselect';
import {SelectButtonModule} from 'primeng/components/selectbutton/selectbutton';
// import {DataTableModule} from 'primeng/components/datatable/datatable';
import {DragulaModule} from "ng2-dragula";
import {DataTableModule} from 'primeng/components/datatable/datatable';
import {SharedModule} from 'primeng/components/common/shared';
import {DateFromToComponent} from './date-from-to/date-from-to.component';
import { OrderModalStatusComponent } from '../customer/order-modal-status/OrderModalStatus.component';
import { ConvertCustomerComponent } from '../customer/convert-customer/convert-customer.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {HttpClient, HttpClientModule} from "@angular/common/http";

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  imports: [
    CommonModule,
    DragulaModule,
    ApiModuleModule,
    FormsModule,
    NgxDatatableModule,
    TooltipModule,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    InputSwitchModule,
    DropdownModule,
    DataTableModule, SharedModule,
    MultiSelectModule,
    SelectButtonModule,
    NgbModule.forRoot(),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  declarations: [
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    PromptComponent,
    SearchComponent,
    AddEditUserModalComponent,
    EditOrderModalComponent,
    ConvertCustomerComponent,
    OrderModalStatusComponent,
    GenerateEditOrderFormComponent,
    DateFromToComponent
    // AddNewCustomerComponent
  ],
  exports: [
    MainHeaderComponentComponent,
    FooterComponentComponent,
    AlertComponentComponent,
    NgxDatatableModule,
    NgbModule,
    ApiModuleModule,
    TooltipModule,
    SearchComponent,
    SplitButtonModule,
    CalendarModule,
    GrowlModule,
    AccordionModule,
    MessagesModule,
    MessageModule,
    FileUploadModule,
    ProgressSpinnerModule,
    GenerateEditOrderFormComponent,
    DragulaModule,
    FormsModule,
    InputSwitchModule,
    DataTableModule, SharedModule,
    DropdownModule,
    MultiSelectModule,
    DateFromToComponent,
    SelectButtonModule
  ],
  entryComponents: [
    PromptComponent,
    AddEditUserModalComponent,
    EditOrderModalComponent,
    ConvertCustomerComponent,
    OrderModalStatusComponent
  ]
})
export class SharedModuleModule {

  static forRoot(entryComponents?: Array<Type<any> | any[]>): ModuleWithProviders {
    return {
      ngModule: SharedModuleModule,
      providers: [
        AlertServiceService,
        CustomerCrudService
      ]
    };
  }
}

export const rootShared: ModuleWithProviders = SharedModuleModule.forRoot();

